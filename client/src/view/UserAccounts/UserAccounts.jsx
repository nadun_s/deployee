import React from 'react';
import TextField from 'material-ui/TextField';
import { orange500, blue500, cyan800, grey900 } from 'material-ui/styles/colors';
import PasswordField from 'material-ui-password-field'
import { Card, CardHeader, CardBody, Row, Col } from 'reactstrap';
import Button from 'material-ui/FlatButton';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import { Table } from 'reactstrap';
import MenuItem from 'material-ui/MenuItem';
import axios from 'axios';
class Account extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            password: "",
            confirmedPassword: "",
            empID: "",
            role: "",
            accessLevel: "",
            status: "",
            uAccessLevel: "",
            createdDate: "",
            createdUser: "",
            updatedDate: "",
            editedUser: "",
            showErr: true,
            errMg: '',
            userArray:[],

        };
        this.handleDate = this.handleDate.bind(this);
        this.SearchUser = this.SearchUser.bind(this);
        this.AddUser = this.AddUser.bind(this);
        this.UpdateUser = this.UpdateUser.bind(this);
        this.Deativate = this.Deativate.bind(this);
        this.Activate = this.Activate.bind(this);
        this.RemoveUser = this.RemoveUser.bind(this);
        this.geneateUserTable = this.geneateUserTable.bind(this);
        

    }

    SearchUser() {
        var id = this.state.empID;

       if(id.length>0){
        var self = this;
        axios.get('/get-user/' + id)
            .then(res => {
                if (res.data.status == 204) {
                    self.setState({ errMg: res.data.message })
                } else {
                    var data = res.data.data;
                    console.log(res.data);
                    self.setState({ errMg: res.data.message })
                    self.setState({ name: data.employee.name })
                    self.setState({ password: data.password })
                    self.setState({ accessLevel: data.accessLevel });
                    self.setState({ createdUser: data.created_usr })
                    self.setState({ edited_usr: data.edited_usr })
                    self.setState({ createdDate: data.created_date })
                    self.setState({ updatedDate: data.edited_date })

                    if (data.status == true) {
                        self.setState({ status: 'Activated' })
                    } else {
                        self.setState({ status: 'Deactivated' })
                    }
                }
            })

       }
       else{
           alert('Please enter Employee ID')
       }
    }

    // Add User
    AddUser() {
        var self = this;
        axios.post('/add-user', {

            empID: this.state.empID,
            password: this.state.password,
            role: this.state.role,
            accessLevel: this.state.accessLevel,
            status: "true",
            created_usr: 'HS2', //this.state.created_usr  sessionStorage.

        })
            .then(function (res) {
                console.log(res.data);
                // this.setState({replyFromSampathBank:res.data.reply})
                //   sessionStorage.setItem('loging_status',res.data.loging_status)
                self.setState({ errMg: res.data.message })

            }).catch(function (error) {
                console.log(error);
            });
    }

    //Update user

    UpdateUser() {
        var self = this;
        axios.post('/edit-user/' + this.state.empID, {

            password: this.state.password,
            role: this.state.role,
            accessLevel: this.state.accessLevel,
            status: "true",
            edited_usr: 'HS2', //this.state.created_usr  sessionStorage.
            edited_date: new Date(),


        })
            .then(function (res) {
                console.log(res.data);
                // this.setState({replyFromSampathBank:res.data.reply})
                //   sessionStorage.setItem('loging_status',res.data.loging_status)
                self.setState({ errMg: res.data.message })

            }).catch(function (error) {
                console.log(error);
            });
    }
    //Deactivate user
    Deativate() {
        var self = this;
        axios.post('/deactivate-user/' + this.state.empID, {
            status: false,
            edited_usr: 'HS2',
            edited_date: new Date(),

        })
            .then(function (res) {
                console.log(res.data);
                // this.setState({replyFromSampathBank:res.data.reply})
                //   sessionStorage.setItem('loging_status',res.data.loging_status)
                self.setState({ errMg: res.data.message })

            }).catch(function (error) {
                console.log(error);
            });
    }
    //Activate
    Activate() {
        var self = this;
        axios.post('/activate-user/' + this.state.empID, {
            status: true,
            edited_usr: 'HS2',
            edited_date: new Date(),

        })
            .then(function (res) {
                console.log(res.data);
                // this.setState({replyFromSampathBank:res.data.reply})
                //   sessionStorage.setItem('loging_status',res.data.loging_status)
                self.setState({ errMg: res.data.message })

            }).catch(function (error) {
                console.log(error);
            });
    }

    //Remove User
    RemoveUser() {
        var self = this;
        axios.delete('/delete-user/' + this.state.empID, {

            edited_usr: 'HS2',
            edited_date: new Date(),

        })
            .then(function (res) {
                console.log(res.data);
                // this.setState({replyFromSampathBank:res.data.reply})
                //   sessionStorage.setItem('loging_status',res.data.loging_status)
                self.setState({ errMg: res.data.message })

            }).catch(function (error) {
                console.log(error);
            });
    }

    componentWillMount() {
        //set variable data
        this.setState({created_usr:sessionStorage.getItem("empid")})
       
        this.setState({UpdateUser:sessionStorage.getItem("empid")})
        this.setState({ uAccessLevel: sessionStorage.getItem("accesLevel") })
        
       if(this.uAccessLevel  >10){
        fetch('/get-user')
        .then(res => res.json())
        .then(data => {
            this.setState({ userArray: data["data"] })
        })
        .catch((err) => {
            console.log(err)
        })
       }
    }

    //user table
    geneateUserTable() {
        {
            return (
                this.state.userArray.map(item =>
                    <tr>
                        {/* <th scope="row">{item.index}</th> */}
                        <td>{item.empID}</td>
                        <td>{item.name}</td>
                        <td>{item.role}</td>
                        <td>{item.accessLevel}</td>
                        <td>{item.status}</td>
                        <td>{item.created_date}</td>
                        <td>{item.created_usr}</td>
                        <td>{item.edited_usr}</td>
                        <td>{item.edited_date}</td>
                     
                        

                    </tr>
                )
            )
        }
    }
    change = e => {

        this.setState({
            [e.target.name]: e.target.value
        });
    };
    //for date 
    handleDate(event, date) {
        this.setState({ date_schedule: date })
    }
    //for select field role
    handleChangeRole = (event, index, role) => this.setState({ role });
    //for select field gender
    handleChangeGender = (event, index, gender) => this.setState({ gender });

    //****************************** for password field */
    handleMouseDownPassword = event => {
        event.preventDefault();
    };

    handleClickShowPassword = () => {
        this.setState({ showPassword: !this.state.showPassword });
    };
    //**************************************************************    
    render() {


        const styles = {
            errorStyle: {
                color: orange500,
            },
            underlineStyle: {
                borderColor: "#01579B",
            },
            floatingLabelStyle: {
                color: "#212121",
            },
            floatingLabelFocusStyle: {
                color: "#212121",
            },
            butyButton: {
                background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                width: 250,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .30)',
            }
        };

        return (
            <div className="card_contenet">
                <Row>
                    <Col md={11} xs={12}>
                        <div className='mainGrid'>
                            <Card className="card">
                                <CardHeader>
                                    <h3> Access Management</h3>
                                </CardHeader>
                                <CardBody>

                                    <div className="text_feild_items">
                                        <div className="inlineBUtton">
                                            <TextField
                                                name="empID"
                                                hintText="Enter Employee ID"
                                                floatingLabelText="Employee ID"
                                                underlineStyle={styles.underlineStyle}
                                                errorStyle={styles.errorStyle}
                                                value={this.state.empID}
                                                onChange={e => this.change(e)}

                                            /><div id="serachBut">
                                                <Button
                                                    label="Search User"
                                                    labelPosition="before"
                                                    primary={true}
                                                    // style={styles.butyButton}
                                                    onClick={this.SearchUser}

                                                />
                                            </div>
                                        </div>
                                        <br />
                                        <TextField
                                            name="name"
                                            hintText="Enter user's Name"
                                            floatingLabelText="Name"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.name}
                                            onChange={e => this.change(e)}

                                        />

                                        {/* <PasswordField
                                        name="password"
                                        // hintText="Enter your password"
                                        // floatingLabelText="Password"
                                        value={this.state.password}
                                        onChange={e => this.change(e)}
                                        // underlineStyle={styles.underlineStyle}
                                    //  errorText="Your password is too short"
                                    // floatingLabelFixed
                                    /> */}
                                        <TextField
                                            name="password"
                                            hintText="Enter user's password"
                                            floatingLabelText="Password"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.password}
                                            onChange={e => this.change(e)}
                                            type="password"

                                        />

                                        <TextField
                                            name="accessLevel"
                                            hintText="Enter user's AccessLevel"
                                            floatingLabelText="Access Level"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.accessLevel}
                                            onChange={e => this.change(e)}


                                        />
                                        <TextField
                                            name="status"
                                            floatingLabelText="Account Status"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.status}
                                            onChange={e => this.change(e)}
                                            disabled

                                        />
                                        <SelectField
                                            floatingLabelText="Role"
                                            value={this.state.role}
                                            onChange={this.handleChangeRole}
                                            underlineStyle={styles.underlineStyle}

                                        >
                                            <MenuItem value={"admin"} primaryText="Admin" />
                                            <MenuItem value={"hr_admin"} primaryText="HR Admin" />
                                            <MenuItem value={"guest"} primaryText="Guest" />
                                            <MenuItem value={"doctor"} primaryText="Doctor" />

                                        </SelectField>
                                        <TextField
                                            name="createdDate"
                                            floatingLabelText="Created date"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.createdDate}
                                            onChange={e => this.change(e)}
                                            disabled

                                        />
                                        <TextField
                                            name="createdUser"
                                            floatingLabelText="Created User"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.createdUser}
                                            onChange={e => this.change(e)}
                                            disabled

                                        />
                                        <TextField
                                            name="updatedDate"
                                            floatingLabelText="Edited User"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.updatedDate}
                                            onChange={e => this.change(e)}
                                            disabled

                                        />
                                        <TextField
                                            name="editedUser"
                                            floatingLabelText="Edited User"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.editedUser}
                                            onChange={e => this.change(e)}
                                            disabled

                                        />
                                        <div className="beutyButDivUpper">
                                        {(4 < this.state.uAccessLevel) &&   <Button
                                                label="Add User"
                                                labelPosition="before"
                                                primary={true}
                                                onClick={this.AddUser}

                                            />
                                        }
                                            {(9 < this.state.uAccessLevel) && <Button
                                                label="Edit User"
                                                labelPosition="before"
                                                primary={true}
                                                onClick={this.UpdateUser}

                                            />}

                                            {(9 < this.state.uAccessLevel) &&
                                                <Button
                                                    label="Remove User"
                                                    labelPosition="before"
                                                    primary={true}
                                                    onClick={this.RemoveUser}

                                                />}
                                          {(9 < this.state.uAccessLevel) &&   <Button
                                                label="Deactivate"
                                                labelPosition="before"
                                                primary={true}
                                                onClick={this.Deativate}
                                            />
                                          }

                                          {(9 < this.state.uAccessLevel) &&   <Button
                                                label="Activate"
                                                labelPosition="before"
                                                primary={true}
                                                onClick={this.Activate}
                                            />
                                          }


                                        </div>
                                    </div>
                                    {this.state.showErr &&
                                        <h3>{this.state.errMg}</h3>
                                    }
                                </CardBody>
                            </Card>


                        </div>

                    </Col>
                </Row>
                <Row>
                    <Col md={11} xs={12}>
                        <div className='mainGrid'>
                            <Card className="card">
                                <CardHeader>
                                    <h3> All User Details</h3>
                                </CardHeader>
                                <CardBody>

                                <Table borderless>
                                    <thead>
                                        <tr>
                                            <th>EmpId</th>
                                            <th>Name</th>
                                            <th>Role</th>
                                            <th>Access Level</th>
                                            <th>Status</th>
                                            <th>Created Date</th>
                                            <th>Created User</th>
                                            <th>Edited Date</th>
                                            <th>Edited User</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.geneateUserTable()
                                        }

                                    </tbody>
                                </Table>
                                  
                                </CardBody>
                            </Card>


                        </div>

                    </Col>
                </Row>



            </div>

        )
    }

}
export default Account;