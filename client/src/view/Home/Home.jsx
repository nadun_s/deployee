import React,{Component} from 'react';
import {FormGroup, FormControl,Button,ControlLabel} from 'reactstrap';
import '../../assets/css/home.css';
import axios from 'axios'
import CardMain from '../../components/cardMain/cardMain'
import { orange500, blue500, cyan800, grey900 } from 'material-ui/styles/colors';
export default class Home extends Component {

    constructor (props){
        super(props);

        this.state={
            username:'',
            password:'',
        }

    }

    
    handleChange = event => {
        this.setState({
          [event.target.id]: event.target.value
        });
      }
    

    render(){

        const styles = {
            errorStyle: {
                color: orange500,
            },
            underlineStyle: {
                borderColor: "#01579B",
            },
            floatingLabelStyle: {
                color: "#212121",
            },
            floatingLabelFocusStyle: {
                color: "#212121",
            },
            butyButton: {
                background: 'linear-gradient(45deg, #24C6DC 30%, #514A9D 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                width: 250,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .30)',
            }
        };
        return(
            <div className="homeMain">
<CardMain />
                </div>
        )
    }
}

