import React from 'react';
import TextField from 'material-ui/TextField';
import { orange500, blue500, cyan800, grey900 } from 'material-ui/styles/colors';
import PasswordField from 'material-ui-password-field'
import { Card, CardHeader, CardBody, Row, Col } from 'reactstrap';
import Button from 'material-ui/FlatButton';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
class Account extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            password: "",
            confirmedPassword: "",
            empID: "",
            role: null,
            gender: null,
            accessLevel: null,
            status: null,
            address: null,
            nic: null,
            phone: null,
            photo: null,
            dob: null,
            uAccessLevel:null,

        };
        this.handleDate = this.handleDate.bind(this);
    }
    componentWillMount(){
        this.setState({uAccessLevel:8})
    }

    change = e => {

        this.setState({
            [e.target.name]: e.target.value
        });
    };
    //for date 
    handleDate(event, date) {
        this.setState({ date_schedule: date })
    }
    //for select field role
    handleChangeRole = (event, index, role) => this.setState({ role });
    //for select field gender
    handleChangeGender = (event, index, gender) => this.setState({ gender });

    //****************************** for password field */
    handleMouseDownPassword = event => {
        event.preventDefault();
      };
    
      handleClickShowPassword = () => {
        this.setState({ showPassword: !this.state.showPassword });
      };
//**************************************************************    
    render() {


        const styles = {
            errorStyle: {
                color: orange500,
            },
            underlineStyle: {
                borderColor: "#01579B",
            },
            floatingLabelStyle: {
                color: "#212121",
            },
            floatingLabelFocusStyle: {
                color: "#212121",
            },
            butyButton: {
                background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                width: 250,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .30)',
            }
        };
        
        return (
            <div>
                <Row>
                    <Col md={10} xs={12}>
                        <div className='mainGrid'>
                            <Card className="card">
                                <CardHeader>
                                    <h3>  Employee Personala Details</h3>
                                </CardHeader>
                                <CardBody>

                                    <div className="text_feild_items">
                                        <div className="inlineBUtton">
                                            <TextField
                                                name="empID"
                                                hintText="Enter Employee ID"
                                                floatingLabelText="Employee ID"
                                                underlineStyle={styles.underlineStyle}
                                                errorStyle={styles.errorStyle}
                                                value={this.state.empID}
                                                onChange={e => this.change(e)}

                                            /><div id="serachBut">
                                                <Button
                                                    label="Search User"
                                                    labelPosition="before"
                                                    primary={true}
                                                // style={styles.butyButton}
                                                // onClick={this.enable_maintenace_mode}

                                                />
                                            </div>
                                        </div>
                                        <br />
                                        <TextField
                                            name="name"
                                            hintText="Enter your Name"
                                            floatingLabelText="Name"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.name}
                                            onChange={e => this.change(e)}

                                        /><DatePicker floatingLabelText="Date of Birth" mode="landscape"
                                            underlineStyle={styles.underlineStyle}
                                            onChange={this.handleDate}
                                            value={this.state.dob}
                                        />
                                        <SelectField
                                            floatingLabelText="Gender"
                                            value={this.state.gender}
                                            onChange={this.handleChangeGender}
                                    
                                            underlineStyle={styles.underlineStyle} >
                                            <MenuItem value={"male"} primaryText="Male" />
                                            <MenuItem value={"female"} primaryText="Female" />


                                        </SelectField>
                                        <TextField
                                            name="nic"
                                            hintText="Enter your NIC"
                                            floatingLabelText="NIC"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.nic}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="address"
                                            hintText="Enter your Address"
                                            floatingLabelText="Address"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.address}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="phone"
                                            hintText="Enter your Phone"
                                            floatingLabelText="Phone"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.phone}
                                            onChange={e => this.change(e)}

                                        /><br />
                                        <br />
                                        <div className="beutyButDivUpper">
                                            <Button
                                                label="Add Employee"
                                                labelPosition="before"
                                                primary={true}

                                            // onClick={this.enable_maintenace_mode}

                                            />
                                            <Button
                                                label="Update Employee"
                                                labelPosition="before"
                                                primary={true}

                                            // onClick={this.enable_maintenace_mode}

                                            />
                                            {(4<this.state.uAccessLevel) && 
                                            <Button
                                                label="Remove Employee"
                                                labelPosition="before"
                                                primary={true}

                                            // onClick={this.enable_maintenace_mode}

                                            />}


                                        </div>

                                        <br />


                                    </div>
                                </CardBody>
                            </Card>

                            <Card className="card">
                                <CardHeader>
                                    <h3> Access Management</h3>
                                </CardHeader>
                                <CardBody>
                       
                                <div className="text_feild_items">
    
                                   
                                        
                                <PasswordField
                                        name="password"
                                        hintText="Enter your password"
                                        floatingLabelText="Password"
                                        value={this.state.password}
                                        onChange={e => this.change(e)}
                                        underlineStyle={styles.underlineStyle}
                                     errorText="Your password is too short"
                                    floatingLabelFixed
                                    />
                                
                                    <TextField
                                        name="role"
                                        hintText="Enter your Name"
                                        floatingLabelText="Name"
                                        underlineStyle={styles.underlineStyle}
                                        errorStyle={styles.errorStyle}
                                        value={this.state.name}
                                        onChange={e => this.change(e)}

                                    />
                                  <SelectField
                                        floatingLabelText="Role"
                                        value={this.state.role}
                                        onChange={this.handleChangeRole}
                                        underlineStyle={styles.underlineStyle}

                                    >
                                        <MenuItem value={"admin"} primaryText="Admin" />
                                        <MenuItem value={"hr_admin"} primaryText="HR Admin" />
                                        <MenuItem value={"guest"} primaryText="Guest" />
                                        <MenuItem value={"doctor"} primaryText="Doctor" />

                                    </SelectField>
                                    <div className="beutyButDivUpper">
                                        <Button
                                            label="Add User"
                                            labelPosition="before"
                                            primary={true}

                                        // onClick={this.enable_maintenace_mode}

                                        />
                                        <Button
                                            label="Update User"
                                            labelPosition="before"
                                            primary={true}

                                        // onClick={this.enable_maintenace_mode}

                                        />
                                        <Button
                                            label="Remove User"
                                            labelPosition="before"
                                            primary={true}
                                        // onClick={this.enable_maintenace_mode}

                                        />

                                        <Button
                                            label="Deactivate"
                                            labelPosition="before"
                                            primary={true}
                                        // onClick={this.enable_maintenace_mode}
                                        />

                                        <Button
                                            label="Activate"
                                            labelPosition="before"
                                            primary={true}
                                        // onClick={this.enable_maintenace_mode}
                                        />

 </div>


                                    </div>


                                </CardBody>
                            </Card>
                        </div>

                    </Col>
                </Row>



            </div>

        )
    }

}
export default Account;