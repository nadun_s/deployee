import React from 'react';
import TextField from 'material-ui/TextField';
import { orange500, blue500, cyan800, grey900 } from 'material-ui/styles/colors';
import PasswordField from 'material-ui-password-field'
import { Card, CardHeader, CardBody, Row, Col } from 'reactstrap';
import Button from 'material-ui/FlatButton';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import axios from 'axios';
import { Table } from 'reactstrap';



class Account extends React.Component {
    constructor(props) {
        super(props);
        this.state = {


            did: "",
            depname: "",
            no_of_wards: "",
            no_of_max_patients: "",
            allocation: "",
            phonenumber: "",
            email: "",
            head_doctor: "",
            status: "",
            services: "",
            create_date: "",
            create_user: "",
            edited_date: "",
            edited_user: "",
            message: "",
            departments:[]

        };
        this.handleDate = this.handleDate.bind(this);
        this.addDepartment = this.addDepartment.bind(this);
        this.clear = this.clear.bind(this);
        this.getalldt = this.getalldt.bind(this);
        this.getewId=this.getewId.bind(this);
        this.updateDepartment=this.updateDepartment.bind(this);
        this.removeDepartment=this.removeDepartment.bind(this);
        this.getdepnamedt=this.getdepnamedt.bind(this);
        this.getdepnamedt=this.getdepnamedt.bind(this);
        this.geneatedepartmentTable=this.geneatedepartmentTable.bind(this);
    }
    componentWillMount() {
        this.getewId();
        this.getalldt();
        this.getAlldepdt();
    }
    clear() {


        this.setState({

            did: "",
            depname: "",
            no_of_wards: "",
            no_of_max_patients: "",
            allocation: "",
            phonenumber: "",
            email: "",
            head_doctor: "",
            status: "",
            services: "",
            create_date: "",
            create_user: "",
            edited_date: "",
            edited_user: "",
            message: ""
        })
        this.getewId();
    }
    addDepartment() {

        //alert(this.state.services)

        var self = this;

        axios.post('http://localhost:3000/departments/create', {

            did: this.state.did,
            depname: this.state.depname,
            no_of_wards: this.state.no_of_wards,
            allocation: this.state.allocation,
            phonenumber: this.state.phonenumber,
            email: this.state.email,
            head_doctor: this.state.head_doctor,
            status: this.state.status,
            services: this.state.services,
            create_date: this.state.create_date,
            create_user: this.state.create_user,
            edited_date: this.state.edited_date,
            edited_user: this.state.edited_user








        })
            .then(res => {
                this.setState({ message: res.data.message })

                console.log(res);
                console.log(res.data);
                this.getewId();
            })
    }
    updateDepartment() {

        //alert(this.state.services)

        var self = this;

        axios.put('/update/departments/'+this.state.did, {

            did: this.state.did,
            depname:self.state.depname,
            no_of_wards: self.state.no_of_wards,
            allocation: self.state.allocation,
            phonenumber: self.state.phonenumber,
            email: self.state.email,
            head_doctor: self.state.head_doctor,
            status: self.state.status,
            services: self.state.services,
            create_date: self.state.create_date,
            create_user: self.state.create_user,
            edited_date: self.state.edited_date,
            edited_user: self.state.edited_user








        })
            .then(res => {
                this.setState({ message: res.data.message })

                console.log(res);
                console.log(res.data);
                this.getewId();
            })
    }

    removeDepartment() {

        //alert(this.state.services)

        var self = this;

        axios.put('/delete/departments/'+this.state.did, {

        })
            .then(res => {
                this.setState({ message: res.data.message })

                console.log(res);
                console.log(res.data);
                this.getewId();
            })
    }

    getewId(){
        axios.get('http://localhost:3000/get-newID')
 .then(res => {
        console.log(res.data);
        this.setState({did: res.data});
        
      }).catch((err) => {
          console.log(err)
      })
    }

    getalldt() {
        axios.get('http://localhost:3000/departments')
          .then(res => {
          this.setState({
            did: res.data.did,
            depname: res.data.depname,
           // email: response.data[0].email,
          })
          })
      }
      getAlldepdt(){
        var url = 'http://localhost:3000/departments';
        console.log(url);
        axios.get(url)
        .then(res => {  
            console.log('this is start')
                console.log(res.data.departments.departments);
                this.setState({  departments:res.data.departments.departments });
                console.log(this.state.departments);
                // this.geneateEmployeeTable();
            }).catch((err) => {
                console.log(err)
            })
    }
    geneatedepartmentTable() {
        {
            return (
                this.state.departments.map(emp =>
                    <tr>
                        <th scope="row">{emp.did}</th>
                        <td>{emp.depname}</td>
                        <td>{emp.no_of_wards}</td>
                        <td>{emp.no_of_max_patients}</td>
                        <td>{emp.allocation}</td>
                        <td>{emp.phonenumber}</td>
                        <td>{emp.email}</td>
                        <td>{emp.head_doctor}</td>
                        <td>{emp.services}</td>
                        {/* <td><span id="remove"><MdClear size={24} color="red" onClick={this.removeFromCart.bind(item)} /></span></td> */}

                    </tr>
                )
            )
        }
    }
      getdepnamedt() {
          var self=this;
          console.log(this.state.depname);
        axios.get('http://localhost:3000/departments/'+this.state.depname)

        
          .then(res => {
            //console.log(res);
                console.log(res.data.deparment.deparment);
                if(res.data.deparment.deparment!=""){
                res.data.deparment.deparment.map(det=>{

                        this.setState({
                            did: det.did,
            depname:det.depname,
            no_of_wards: det.no_of_wards,
            allocation: det.allocation,
            phonenumber: det.phonenumber,
            email: det.email,
            head_doctor: det.head_doctor,
            status: det.status,
            services: det.services,
            create_date: det.create_date,
            create_user: det.create_user,
            edited_date: det.edited_date,
            edited_user: det.edited_user


                        });
                    


                })}
                else{
                    this.setState({message:'invalid search'});
                }

                
          })
      }
     
    // updateDepartment() {

    //     //alert(this.state.services)

    //     var self = this;

    //     axios.put('http://localhost:3000/gen-information/update/'+this.state.depname, {

    //         did: this.state.did,
    //         depname: this.state.depname,
    //         no_of_wards: this.state.no_of_wards,
    //         allocation: this.state.allocation,
    //         phonenumber: this.state.phonenumber,
    //         email: this.state.email,
    //         head_doctor: this.state.head_doctor,
    //         status: this.state.status,
    //         services: this.state.services,
    //         create_date: this.state.create_date,
    //         create_user: this.state.create_user,
    //         edited_date: this.state.edited_date,
    //         edited_user: this.state.edited_user








    //     })
    //         .then(res => {
    //             //this.setState({message:res.data.message})

    //             console.log(res);
    //             console.log(res.data);
    //         })
    // }




    change = e => {

        this.setState({
            [e.target.name]: e.target.value
        });
    };
    //for date 
    handleDate(event, date) {
        this.setState({ date_schedule: date })
    }
    //for select field role
    handleChangeRole = (event, index, role) => this.setState({ role });
    //for select field gender
    handleChangeGender = (event, index, gender) => this.setState({ gender });

    //****************************** for password field */
    handleMouseDownPassword = event => {
        event.preventDefault();
    };

    handleClickShowPassword = () => {
        this.setState({ showPassword: !this.state.showPassword });
    };
    //**************************************************************    
    render() {


        const styles = {
            errorStyle: {
                color: orange500,
            },
            underlineStyle: {
                borderColor: "#01579B",
            },
            floatingLabelStyle: {
                color: "#212121",
            },
            floatingLabelFocusStyle: {
                color: "#212121",
            },
            butyButton: {
                background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                width: 250,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .30)',
            }
        };

        return (
            <div>
                <Row>
                    <Col md={10} xs={12}>
                        <div className='mainGrid'>
                            <Card className="card">
                                <CardHeader>
                                    <h3>  Department information</h3>
                                </CardHeader>
                                <CardBody>

                                    <div className="text_feild_items">
                                        <div className="inlineBUtton">
                                            <TextField
                                                name="did"
                                                hintText=""
                                                floatingLabelText="department Id"
                                                underlineStyle={styles.underlineStyle}
                                                errorStyle={styles.errorStyle}
                                                value={this.state.did}
                                                onChange={e => this.change(e)}

                                            />

                                        </div>
                                        <br />
                                        <div className="inlineBUtton">
                                            <TextField
                                                name="depname"
                                                hintText="Enter depname"
                                                floatingLabelText="department name"
                                                underlineStyle={styles.underlineStyle}
                                                errorStyle={styles.errorStyle}
                                                value={this.state.depname}
                                                onChange={e => this.change(e)}

                                            />
                                            <Button
                                                label="Search by name"
                                                labelPosition="before"
                                                primary={true}
                                                onClick={this.getdepnamedt}

                                            // onClick={this.enable_maintenace_mode}

                                            />
                                        </div>
                                        <TextField
                                            name="no_of_wards"
                                            hintText="Enter no of wards"
                                            floatingLabelText="no of wards"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.no_of_wards}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="no_of_max_patients"
                                            hintText="Enter no of max patients in this department"
                                            floatingLabelText="no_of_max_patients"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.no_of_max_patients}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="allocation"
                                            hintText="Enter allocation"
                                            floatingLabelText="allocation"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.allocation}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="phonenumber"
                                            hintText="Enter your phonenumber"
                                            floatingLabelText="phonenumber"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.phonenumber}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="email"
                                            hintText="Enter  email"
                                            floatingLabelText="email"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.email}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="head_doctor"
                                            hintText="Enter head_doctor"
                                            floatingLabelText="head doctor name"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.head_doctor}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="services"
                                            hintText="Enter services"
                                            floatingLabelText="services"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.services}
                                            onChange={e => this.change(e)}

                                        />

                                        <br />
                                        <br />
                                        <div className="beutyButDivUpper">
                                            <Button
                                                label="Add department"
                                                labelPosition="before"
                                                primary={true}
                                                onClick={this.addDepartment}



                                            />

                                            <Button
                                                label="Update department"
                                                labelPosition="before"
                                                primary={true}
                                                onClick={this.updateDepartment}

                                            // onClick={this.enable_maintenace_mode}

                                            />



                                           
                                                <Button
                                                    label="Remove department"
                                                    labelPosition="before"
                                                    primary={true}
                                                    onClick={this.removeDepartment}

                                                // onClick={this.enable_maintenace_mode}

                                                />
                                            <Button
                                                label="clear"
                                                labelPosition="before"
                                                primary={true}
                                                onClick={this.clear}




                                            />
                                             <br /> <br />

                                            <h3>{this.state.message}</h3>




                                        </div>

                                        <br />


                                    </div>
                                </CardBody>
                            </Card>
                            <Card className="card">
                                <CardHeader>
                                    <h3> department Management</h3>
                                </CardHeader>
                                <CardBody>
                       
                                <div className="text_feild_items">
    
                                <div>
                                <Table border="1px">
                                <thead>
                                    <tr>
                                        <th>did</th>
                                        <th>depName</th>
                                        <th>no_of wards</th>
                                        <th>no of max patients      </th>
                                        <th>allocation</th>
                                        <th>phone number</th>
                                        <th>email</th>
                                        <th>Contact Number</th>
                                        <th>head doctor</th>
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                       this.geneatedepartmentTable()
                                    }

                                </tbody>
                            </Table>
            </div>
                                        
                                
                                
                                    
                                  
                                    <div className="beutyButDivUpper">
                                       
                                      

 </div>


                                    </div>


                                </CardBody>
                            </Card>


                        </div>

                    </Col>
                </Row>



            </div>

        )
    }

}
export default Account;