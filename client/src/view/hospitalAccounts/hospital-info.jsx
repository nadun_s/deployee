import React from 'react';
import TextField from 'material-ui/TextField';
import { orange500, blue500, cyan800, grey900 } from 'material-ui/styles/colors';
import PasswordField from 'material-ui-password-field'
import { Card, CardHeader, CardBody, Row, Col } from 'reactstrap';
import Button from 'material-ui/FlatButton';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import axios from 'axios';
import { Table } from 'reactstrap';



class Account extends React.Component {
    constructor(props) {
        super(props);
        this.state = {


            branch_id: "",
            hos_name: "",
            no_of_staffs: "",
            hos_no: "",
            hos_email: "",
            address: "",
           
            status: "",
          
            create_date: "",
            create_user: "",
            edited_date: "",
            edited_user: "",
            departments:[],
            message:""
          

        };
        this.handleDate = this.handleDate.bind(this);
        this.addDepartment = this.addDepartment.bind(this);
        this.clear = this.clear.bind(this);
        this.getalldt = this.getalldt.bind(this);
        this.getewId=this.getewId.bind(this);
        this.updateDepartment=this.updateDepartment.bind(this);
        this.removeDepartment=this.removeDepartment.bind(this);
        this.getdepnamedt=this.getdepnamedt.bind(this);
        this.getdepnamedt=this.getdepnamedt.bind(this);
      //  this.geneatedepartmentTable=this.geneatedepartmentTable.bind(this);
    }
    componentWillMount() {
        this.getewId();
        // this.getalldt();
         this.getAlldepdt();
    }
    clear() {


        this.setState({

            branch_id: "",
            hos_name: "",
            no_of_staffs: "",
            hos_no: "",
            hos_email: "",
            address: "",
           
            status: "",
          
            create_date: "",
            create_user: "",
            edited_date: "",
            edited_user: "",
            message:""
        })
        this.getewId();
    }
    addDepartment() {

        //alert(this.state.services)

        var self = this;

        axios.post('http://localhost:3000/gen-information', {

            branch_id: this.state.branch_id,
            hos_name: this.state.hos_name,
            no_of_staffs:this.state.no_of_staffs,
            hos_no: this.state.hos_no,
            hos_email: this.state.hos_email,
            address: this.state.address,
           
            //services: this.state.services,
            create_date: this.state.create_date,
            create_user: this.state.create_user,
            edited_date: this.state.edited_date,
            edited_user: this.state.edited_user








        })
            .then(res => {
                this.setState({ message: res.data.message })

                console.log(res);
                console.log(res.data);
                this.getewId();
                this.geneatedepartmentTable()
            })
    }
    updateDepartment() {

        //alert(this.state.services)

        var self = this;

        axios.put('/gen-information/update/'+this.state.hos_name, {

            branch_id: this.state.branch_id,
            hos_name: this.state.hos_name,
            no_of_staffs:this.state.no_of_staffs,
            hos_no: this.state.hos_no,
            hos_email: this.state.hos_email,
            address: this.state.address,
           
            //services: this.state.services,
            create_date: this.state.create_date,
            create_user: this.state.create_user,
            edited_date: this.state.edited_date,
            edited_user: this.state.edited_user








        })
            .then(res => {
                this.setState({ message: res.data.message })

                console.log(res);
                console.log(res.data);
                this.getewId();
            })
    }

    removeDepartment() {

        //alert(this.state.services)

        var self = this;

        axios.put('/gen-information/delete/'+this.state.hos_name, {

        })
            .then(res => {
                this.setState({ message: res.data.message })

                console.log(res);
                console.log(res.data);
                this.getewId();
            })
    }

    getewId(){
        axios.get('http://localhost:3000/info/get-newID')
 .then(res => {
        console.log(res.data);
        this.setState({branch_id: res.data});
        
      }).catch((err) => {
          console.log(err)
      })
    }

    getalldt() {
        axios.get('http://localhost:3000/departments')
          .then(res => {
          this.setState({
            did: res.data.did,
            depname: res.data.depname,
           // email: response.data[0].email,
          })
          })
      }
      getAlldepdt(){
        var url = 'http://localhost:3000/gen-information';
        console.log(url);
        axios.get(url)
        .then(res => {  
            console.log('this is start')
            console.log(res.data)
                console.log(res.data.info.info);
                this.setState({  departments:res.data.info.info });
                console.log(this.state.departments);
                // this.geneateEmployeeTable();
            }).catch((err) => {
                console.log(err)
            })
    }
    geneatedepartmentTable() {
        {
            return (
                this.state.departments.map(emp =>
                    <tr>
                        <th scope="row">{emp.branch_id}</th>
                        <td>{emp.hos_name}</td>
                        <td>{emp.no_of_staffs}</td>
                        <td>{emp.hos_no}</td>
                        <td>{emp.hos_email}</td>
                        <td>{emp.address}</td>
                        <td>{emp.create_date}</td>
                       
                        <td>{emp.create_user}</td>
                       
                        {/* <td><span id="remove"><MdClear size={24} color="red" onClick={this.removeFromCart.bind(item)} /></span></td> */}

                    </tr>
                )
            )
        }
    }
      getdepnamedt() {
          var self=this;
          console.log(this.state.hos_name);
        axios.get('http://localhost:3000/gen-information/'+this.state.hos_name)

        
          .then(res => {
            console.log(res.data.deparment.deparment);
                console.log(res.data);
                if(res.data.deparment.deparment!=""){
                res.data.deparment.deparment.map(det=>{

                        this.setState({
                            branch_id: det.branch_id,
                            hos_name:det.hos_name,
                            no_of_staffs:det.no_of_staffs,
                            hos_no: det.hos_no,
                            hos_email: det.hos_email,
                            address: det.address,
                           


                        });


                })}
                else{
                    this.setState({message:'invalid search'})
                }

                
          })
      }
     
    // updateDepartment() {

    //     //alert(this.state.services)

    //     var self = this;

    //     axios.put('http://localhost:3000/gen-information/update/'+this.state.depname, {

    //         did: this.state.did,
    //         depname: this.state.depname,
    //         no_of_wards: this.state.no_of_wards,
    //         allocation: this.state.allocation,
    //         phonenumber: this.state.phonenumber,
    //         email: this.state.email,
    //         head_doctor: this.state.head_doctor,
    //         status: this.state.status,
    //         services: this.state.services,
    //         create_date: this.state.create_date,
    //         create_user: this.state.create_user,
    //         edited_date: this.state.edited_date,
    //         edited_user: this.state.edited_user








    //     })
    //         .then(res => {
    //             //this.setState({message:res.data.message})

    //             console.log(res);
    //             console.log(res.data);
    //         })
    // }




    change = e => {

        this.setState({
            [e.target.name]: e.target.value
        });
    };
    //for date 
    handleDate(event, date) {
        this.setState({ date_schedule: date })
    }
    //for select field role
    handleChangeRole = (event, index, role) => this.setState({ role });
    //for select field gender
    handleChangeGender = (event, index, gender) => this.setState({ gender });

    //****************************** for password field */
    handleMouseDownPassword = event => {
        event.preventDefault();
    };

    handleClickShowPassword = () => {
        this.setState({ showPassword: !this.state.showPassword });
    };
    //**************************************************************    
    render() {


        const styles = {
            errorStyle: {
                color: orange500,
            },
            underlineStyle: {
                borderColor: "#01579B",
            },
            floatingLabelStyle: {
                color: "#212121",
            },
            floatingLabelFocusStyle: {
                color: "#212121",
            },
            butyButton: {
                background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                width: 250,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .30)',
            }
        };

        return (
            <div>
                <Row>
                    <Col md={10} xs={12}>
                        <div className='mainGrid'>
                            <Card className="card">
                                <CardHeader>
                                    <h3>  Hospitl general information</h3>
                                </CardHeader>
                                <CardBody>

                                    <div className="text_feild_items">
                                        <div className="inlineBUtton">
                                            <TextField
                                                name="branch_id"
                                                hintText=""
                                                floatingLabelText="branch Id"
                                                underlineStyle={styles.underlineStyle}
                                                errorStyle={styles.errorStyle}
                                                value={this.state.branch_id}
                                                onChange={e => this.change(e)}

                                            />

                                        </div>
                                        <br />
                                        <div className="inlineBUtton">
                                            <TextField
                                                name="hos_name"
                                                hintText="Enter hospital name"
                                                floatingLabelText="hospital name"
                                                underlineStyle={styles.underlineStyle}
                                                errorStyle={styles.errorStyle}
                                                value={this.state.hos_name}
                                                onChange={e => this.change(e)}
                                                

                                            />
                                              <Button
                                                label="Search by name"
                                                labelPosition="before"
                                                primary={true}
                                                onClick={this.getdepnamedt}

                                            // onClick={this.enable_maintenace_mode}

                                            />
                                         
                                        </div>
                                        <TextField
                                            name="no_of_staffs"
                                            hintText="Enter no of staff"
                                            floatingLabelText="no of staff"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.no_of_staffs}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="hos_no"
                                            hintText="Enter no phone number"
                                            floatingLabelText="no_of_max_patients"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.hos_no}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="hos_email"
                                            hintText="Enter hospital email"
                                            floatingLabelText="email"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.hos_email}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="address"
                                            hintText="Enter your address"
                                            floatingLabelText="address"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.address}
                                            onChange={e => this.change(e)}

                                        />
                                        
                                      
                                     

                                        <br />
                                        <br />
                                        <div className="beutyButDivUpper">
                                            <Button
                                                label="Add hospital info"
                                                labelPosition="before"
                                                primary={true}
                                                onClick={this.addDepartment}
                                               



                                            />

                                            <Button
                                                label="Update hospital information"
                                                labelPosition="before"
                                                primary={true}
                                                onClick={this.updateDepartment}

                                            // onClick={this.enable_maintenace_mode}

                                            />



                                           
                                                <Button
                                                    label="Remove hospital info"
                                                    labelPosition="before"
                                                    primary={true}
                                                    onClick={this.removeDepartment}

                                                // onClick={this.enable_maintenace_mode}

                                                />
                                            <Button
                                                label="clear"
                                                labelPosition="before"
                                                primary={true}
                                                onClick={this.clear}




                                            />
                                            <h3>{this.state.message}</h3>




                                        </div>

                                        <br />


                                    </div>
                                </CardBody>
                            </Card>
                            <Card className="card">
                                <CardHeader>
                                    <h3> hospital information</h3>
                                </CardHeader>
                                <CardBody>
                       
                                <div className="text_feild_items">
    
                                <div>
                                <Table border="1px">
                                <thead>
                                    <tr>
                                        <th>branch Id</th>
                                        <th>hospital name</th>
                                        <th>no of staffs</th>
                                        <th>phone number    </th>
                                        <th>email</th>
                                        <th>address</th>
                                        <th>create date</th>
                                        <th>create_user</th>
                                       
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                       this.geneatedepartmentTable()
                                    }

                                </tbody>
                            </Table>
            </div>
                                        
                                
                                
                                    
                                  
                                    <div className="beutyButDivUpper">
                                       
                                      

 </div>


                                    </div>


                                </CardBody>
                            </Card>


                        </div>

                    </Col>
                </Row>



            </div>

        )
    }

}
export default Account;