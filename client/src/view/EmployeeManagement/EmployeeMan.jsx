import React from 'react';
import TextField from 'material-ui/TextField';
import { orange500, blue500, cyan800, grey900 } from 'material-ui/styles/colors';
import PasswordField from 'material-ui-password-field'
import { Card, CardHeader, CardBody, Row, Col } from 'reactstrap';
import Button from 'material-ui/FlatButton';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import axios from 'axios';
import { Table } from 'reactstrap';
// import React from "react";

class Employee extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // empID: "",
            name: "",
            nic: "",
            dob: "",
            // doj: "",
            designation: "",
            // role: "",
            com_address: "",
            //perm_address: "",
            cur_pto: "",
            con_no: "",
            email: "",
            salary: "",
            created_date: "",
            created_usr: "cccc",
            edited_date: "",
            edited_usr: "",
            message: "",
            dobdis: "",
            employees: [],

        };
        this.handleDate = this.handleDate.bind(this);
        this.addEmployee = this.addEmployee.bind(this);
        this.clearTextFields = this.clearTextFields.bind(this);
        this.clearTextFieldsButton = this.clearTextFieldsButton.bind(this);
        this.getewId = this.getewId.bind(this);
        this.searchEmpByName = this.searchEmpByName.bind(this);
        this.searchEmpByEmpID = this.searchEmpByEmpID.bind(this);
        this.searchEmpByNIC = this.searchEmpByNIC.bind(this);
        this.updateEmployeeDet = this.updateEmployeeDet.bind(this);
        this.removeEmployee = this.removeEmployee.bind(this);
        this.getAllEmployees = this.getAllEmployees.bind(this);
    }
    componentWillMount() {
        this.setState({ uAccessLevel: 8 })
    }

    searchEmpByName() {
        var url = 'http://localhost:3000/emp/search-employeesByname/' + this.state.name;
        console.log(url);
        axios.get(url)
            .then(res => {
                if (res.data.length == 0) {
                    this.setState({ message: "No Employee Found!!!!" });
                }
                res.data.map(det => {
                    console.log(det.empID);
                    if (det.empID == "") {
                        this.setState({ message: "No Employee Found!!!!" });
                    }
                    this.setState({
                        empID: det.empID,
                        name: det.name,
                        nic: det.nic,
                        dob: det.dob,
                        // doj: this.state.showPassword,
                        designation: det.designation,
                        // role: this.state.showPassword,
                        com_address: det.com_address,
                        //perm_address: "",
                        // cur_pto: this.state.showPassword,
                        con_no: det.con_no,
                        email: det.email,
                        salary: det.salary,
                        dobdis: det.dob
                    });
                })
                console.log(res.data);
            }).catch((err) => {
                console.log(err)
            })
    }
    
    getAllEmployees(){
        var url = 'http://localhost:3000/emp/get-employees';
        console.log(url);
        axios.get(url)
        .then(res => {  
                console.log(res.data);
                this.setState({employees: res.data});
                console.log(this.state.employees);
                // this.geneateEmployeeTable();
            }).catch((err) => {
                console.log(err)
            })
    }

    geneateEmployeeTable() {
        {
            return (
                this.state.employees.map(emp =>
                    <tr>
                        <th scope="row">{emp.empID}</th>
                        <td>{emp.name}</td>
                        <td>{emp.nic}</td>
                        <td>{emp.dob}</td>
                        <td>{emp.designation}</td>
                        <td>{emp.com_address}</td>
                        <td>{emp.con_no}</td>
                        <td>{emp.email}</td>
                        <td>{emp.salary}</td>
                        {/* <td><span id="remove"><MdClear size={24} color="red" onClick={this.removeFromCart.bind(item)} /></span></td> */}

                    </tr>
                )
            )
        }
    }

    searchEmpByNIC() {
        var url = 'http://localhost:3000/emp/search-employeesBynic/' + this.state.nic;
        console.log(url);
        axios.get(url)
            .then(res => {
                if (res.data.length == 0) {
                    this.setState({ message: "No Employee Found!!!!" });
                }
                res.data.map(det => {
                    console.log(det.empID);
                    this.setState({
                        empID: det.empID,
                        name: det.name,
                        nic: det.nic,
                        dob: det.dob,
                        // doj: this.state.showPassword,
                        designation: det.designation,
                        // role: this.state.showPassword,
                        com_address: det.com_address,
                        //perm_address: "",
                        // cur_pto: this.state.showPassword,
                        con_no: det.con_no,
                        email: det.email,
                        salary: det.salary,
                        dobdis: det.dob,
                    });
                })
                console.log(res.data);
            }).catch((err) => {
                console.log(err)
            })
    }

    updateEmployeeDet() {
        axios.put('http://localhost:3000/emp/update-employees', {
            empID: this.state.empID,
            name: this.state.name,
            nic: this.state.nic,
            dob: this.state.dob,
            designation: this.state.designation,
            // role: this.state.showPassword,
            com_address: this.state.com_address,
            //perm_address: "",
            // cur_pto: this.state.showPassword,
            con_no: this.state.con_no,
            email: this.state.email,
            salary: this.state.salary,
            // edited_date: this.state.showPassword,
            edited_usr: "nandun",
        })
            .then(response => {
                console.log(response);
                this.setState({ message: response.data });
            })
            .catch(err => {
                console.log(err);
            })
    }

    removeEmployee() {
        axios.put('http://localhost:3000/emp/remove-employees', {
            empID: this.state.empID,
        })
            .then(response => {
                console.log(response);
                this.setState({ message: response.data });
            })
            .catch(err => {
                console.log(err);
            });
    }

    searchEmpByEmpID() {
        // var id = document.getElementById('empID');
        // console.log(id);
        // this.setState({});
        var url = 'http://localhost:3000/emp/search-employeesById/' + this.state.empID;
        console.log(url);
        axios.get(url)
            .then(res => {
                if (res.data.length == 0) {
                    this.setState({ message: "No Employee Found!!!!" });
                }
                res.data.map(det => {
                    console.log(det.empID);
                    console.log(det.dob);
                    this.setState({
                        empID: det.empID,
                        name: det.name,
                        nic: det.nic,
                        dob: det.dob,
                        // doj: this.state.showPassword,
                        designation: det.designation,
                        // role: this.state.showPassword,
                        com_address: det.com_address,
                        //perm_address: "",
                        // cur_pto: this.state.showPassword,
                        con_no: det.con_no,
                        email: det.email,
                        salary: det.salary,
                        dobdis: det.dob,
                    });
                })
                console.log(res.data);
            }).catch((err) => {
                console.log(err)
            })
    }

    componentDidMount() {
        this.getewId();
        this.getAllEmployees();
    }
    getewId() {
        axios.get(`http://localhost:3000/emp/get-newID`)
            .then(res => {
                console.log(res.data);
                this.setState({ empID: res.data });

            }).catch((err) => {
                console.log(err)
            })
    }

    change = e => {

        this.setState({
            [e.target.name]: e.target.value
        });
    };
    //for date 
    handleDate(event, date) {
        this.setState({ dob: date })
    }
    //for select field role
    handleChangeRole = (event, index, role) => this.setState({ role });
    //for select field gender
    handleChangeGender = (event, index, gender) => this.setState({ gender });

    //****************************** for password field */
    handleMouseDownPassword = event => {
        event.preventDefault();
    };

    handleClickShowPassword = () => {
        this.setState({ showPassword: !this.state.showPassword });
    };
    //**************************************************************
    addEmployee = event => {
        event.preventDefault();

        axios.post(`http://localhost:3000/emp/add-employee`, {
            empID: this.state.empID,
            name: this.state.name,
            nic: this.state.nic,
            dob: this.state.dob,
            // doj: this.state.showPassword,
            designation: this.state.designation,
            // role: this.state.showPassword,
            com_address: this.state.com_address,
            //perm_address: "",
            // cur_pto: this.state.showPassword,
            con_no: this.state.con_no,
            email: this.state.email,
            salary: this.state.salary,
            // created_date: this.state.showPassword,
            created_usr: this.state.created_usr,
            // edited_date: this.state.showPassword,
            edited_usr: this.state.showPassword,
        })
            .then(res => {
                this.setState({ message: res.data.message });
                console.log(this.state.message);
                this.getewId();
                this.clearTextFields();
                this.getAllEmployees();
            })
    }
    clearTextFields() {
        this.setState({
            // empID: "",
            name: "",
            nic: "",
            dob: "",
            // doj: "",
            designation: "",
            // role: "",
            com_address: "",
            //perm_address: "",
            cur_pto: "",
            con_no: "",
            email: "",
            salary: "",
            created_date: "",
            created_usr: "cccc",
            edited_date: "",
            edited_usr: "",
            // message:"",
            dobdis: "",
        });
    }
    clearTextFieldsButton() {
        this.setState({
            // empID: "",
            name: "",
            nic: "",
            dob: "",
            // doj: "",
            designation: "",
            // role: "",
            com_address: "",
            //perm_address: "",
            cur_pto: "",
            con_no: "",
            email: "",
            salary: "",
            created_date: "",
            created_usr: "cccc",
            edited_date: "",
            edited_usr: "",
            message: "",
            dobdis: "",
        });
        this.getewId();
    }

    
    render() {


        const styles = {
            errorStyle: {
                color: orange500,
            },
            underlineStyle: {
                borderColor: "#01579B",
            },
            floatingLabelStyle: {
                color: "#212121",
            },
            floatingLabelFocusStyle: {
                color: "#212121",
            },
            butyButton: {
                background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                width: 250,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .30)',
            }
        };

        return (
            <div>
                <Row>
                    <Col md={10} xs={12}>
                        <div className='mainGrid'>
                            <Card className="card">
                                <CardHeader>
                                    <h3>  Employee Personala Details</h3>
                                </CardHeader>
                                <CardBody>

                                    <div className="text_feild_items">
                                        <div className="inlineBUtton">
                                            <TextField
                                                name="empID"
                                                hintText="Enter Employee ID"
                                                floatingLabelText="Employee ID"
                                                underlineStyle={styles.underlineStyle}
                                                errorStyle={styles.errorStyle}
                                                value={this.state.empID}
                                                onChange={e => this.change(e)}


                                            /><div id="serachBut">
                                                <Button
                                                    label="Search Employee"
                                                    labelPosition="before"
                                                    primary={true}
                                                    // style={styles.butyButton}
                                                    onClick={this.searchEmpByEmpID}

                                                />
                                            </div>
                                        </div>
                                        <br />
                                        <div className="inlineBUtton">
                                            <TextField
                                                name="name"
                                                hintText="Enter your Name"
                                                floatingLabelText="Name"
                                                underlineStyle={styles.underlineStyle}
                                                errorStyle={styles.errorStyle}
                                                value={this.state.name}
                                                onChange={e => this.change(e)}

                                            /><div id="serachBut">
                                                <Button
                                                    label="Search Employee"
                                                    labelPosition="before"
                                                    primary={true}
                                                    // style={styles.butyButton}
                                                    onClick={this.searchEmpByName}

                                                /></div>
                                        </div>
                                        <div className="inlineBUtton">
                                            <TextField
                                                name="nic"
                                                hintText="Enter NIC"
                                                floatingLabelText="NIC"
                                                underlineStyle={styles.underlineStyle}
                                                errorStyle={styles.errorStyle}
                                                value={this.state.nic}
                                                onChange={e => this.change(e)}

                                            />
                                            <div id="serachBut">
                                                <Button
                                                    label="Search Employee"
                                                    labelPosition="before"
                                                    primary={true}
                                                    // style={styles.butyButton}
                                                    onClick={this.searchEmpByNIC}

                                                /></div></div>

                                        <div className="inlineBUtton">
                                            <DatePicker floatingLabelText="Date of Birth" mode="landscape"
                                                underlineStyle={styles.underlineStyle}
                                                onChange={this.handleDate}
                                                value={this.state.dob}
                                                select={this.state.dobs}
                                            />
                                            <div id="serachBut">
                                                <label>{this.state.dobdis}</label>
                                            </div></div>

                                        {/* <SelectField
                                            floatingLabelText="Gender"
                                            value={this.state.gender}
                                            onChange={this.handleChangeGender}

                                            underlineStyle={styles.underlineStyle} >
                                            <MenuItem value={"male"} primaryText="Male" />
                                            <MenuItem value={"female"} primaryText="Female" />


                                        </SelectField> */}
                                        <TextField
                                            name="designation"
                                            hintText="Enter Designation"
                                            floatingLabelText="Designation"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.designation}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="com_address"
                                            hintText="Enter Address"
                                            floatingLabelText="Address"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.com_address}
                                            onChange={e => this.change(e)}

                                        />
                                        <TextField
                                            name="con_no"
                                            hintText="Enter Phone Number"
                                            floatingLabelText="Phone"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.con_no}
                                            onChange={e => this.change(e)}

                                        /><TextField
                                            name="email"
                                            hintText="Enter E-Mail"
                                            floatingLabelText="E-Mail"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.email}
                                            onChange={e => this.change(e)}

                                        /><TextField
                                            name="salary"
                                            hintText="Enter Salary"
                                            floatingLabelText="Salary"
                                            underlineStyle={styles.underlineStyle}
                                            errorStyle={styles.errorStyle}
                                            value={this.state.salary}
                                            onChange={e => this.change(e)}

                                        /><br />
                                        <br />
                                        <div className="beutyButDivUpper">
                                            <Button
                                                label="Add Employee"
                                                labelPosition="before"
                                                primary={true}

                                                onClick={this.addEmployee}

                                            />
                                            <Button
                                                label="Update Employee"
                                                labelPosition="before"
                                                primary={true}

                                                onClick={this.updateEmployeeDet}

                                            />
                                            {(4 < this.state.uAccessLevel) &&
                                                <Button
                                                    label="Remove Employee"
                                                    labelPosition="before"
                                                    primary={true}

                                                    onClick={this.removeEmployee}

                                                />
                                            }
                                            <Button
                                                label="Clear"
                                                labelPosition="before"
                                                primary={true}

                                                onClick={this.clearTextFieldsButton}

                                            />


                                        </div>

                                        <br />
                                        <br />
                                        <h3>{this.state.message}</h3>

                                    </div>
                                </CardBody>
                            </Card>

                            <Card className="cardTab">
                                <CardHeader>
                                    <h3 align = "center"> Employee Details</h3>
                                </CardHeader>
                                <CardBody>

                                    <Table border = "1px">
                                    <thead>
                                        <tr>
                                            <th align = "left">Employee No</th>
                                            <th align = "left">Name</th>
                                            <th align = "left">NIC</th>
                                            <th align = "left">Date Of Birth</th>
                                            <th align = "left">Designation</th>
                                            <th align = "left">Communication Address</th>
                                            <th align = "left">Contact Number</th>
                                            <th align = "left">E-Mail</th>
                                            <th align = "left">Salary</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.geneateEmployeeTable()
                                        }

                                    </tbody>
                                </Table>
                                  
                                </CardBody>
                            </Card>
                        </div>

                    </Col>

                </Row>



            </div>

        )
    }

}
export default Employee;