import React from 'react';
import TextField from 'material-ui/TextField';
import { orange500, blue500, cyan800, grey900 } from 'material-ui/styles/colors';
import PasswordField from 'material-ui-password-field'
import { Card, CardHeader, CardBody, Row, Col } from 'reactstrap';
import Button from 'material-ui/FlatButton';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import axios from 'axios';
import { Table } from 'reactstrap';
// import React from "react";

class Assign extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            asnID: "",
            empName: "",
            depName: "",
            created_usr: "",
            edited_usr: "",
            message: "",
            assigns:[],
        };
        this.getewId = this.getewId.bind(this);
        this.clearTextFieldsButton = this.clearTextFieldsButton.bind(this);
        this.clearTextFields - this.clearTextFields.bind(this);
        this.searchEmpByAssignID = this.searchEmpByAssignID.bind(this);
        this.searchEmpByEmpName - this.searchEmpByEmpName.bind(this);
        this.updateAssignDet = this.updateAssignDet.bind(this);
        this.removeAssign =this.removeAssign.bind(this);
        this.getAllAssign = this.getAllAssign.bind(this);
    }
    componentWillMount() {
        this.setState({ uAccessLevel: 8 })
    }

    componentDidMount() {
        this.getewId();
        this.getAllAssign();
    }
    addAssign = event => {
        event.preventDefault();

        axios.post(`http://localhost:3000/asn/add-assign`, {
            asnID: this.state.asnID,
            empName: this.state.empName,
            depName: this.state.depName,
            created_usr: "nandun",
        })
            .then(res => {
                console.log(res);
                this.setState({ message: res.data.message });
                console.log(this.state.message);
                console.log(this.state.message);
                this.getewId();
                this.clearTextFields();
                this.geneateAssignTable();
            })
    }
    clearTextFields() {
        this.setState({
            asnID: "",
            empName: "",
            depName: "",
            created_usr: "",
            edited_usr: "",
            message: ""
        });
    }
    clearTextFieldsButton() {
        this.setState({
            asnID: "",
            empName: "",
            depName: "",
            created_usr: "",
            edited_usr: "",
            message: "",
            
        });
        this.getewId();
    }
    getewId() {
        axios.get(`http://localhost:3000/asn/get-newID`)
            .then(res => {
                console.log(res.data);
                this.setState({ asnID: res.data });

            }).catch((err) => {
                console.log(err)
            })
    }

    searchEmpByAssignID() {
        var url = 'http://localhost:3000/asn/search-assignsByAssignId/' + this.state.asnID;
        console.log(url);
        axios.get(url)
            .then(res => {
                if (res.data.length == 0) {
                    this.setState({ message: "No Assing Found!!!!" });
                }
                res.data.map(det => {
                    console.log(det.empID);
                    // console.log(det.dob);
                    this.setState({
                        asnID: det.asnID,
                        empName: det.empName,
                        depName: det.empName,
                    });
                })
                console.log(res.data);
            }).catch((err) => {
                console.log(err)
            })
    }

    searchEmpByEmpName(event) {
        event.preventDefault();
        console.log('afad');
        console.log(this.state.empName);
        var url = 'http://localhost:3000/asn/search-assignsByEmpName/' + this.state.empName;
        console.log(url);
        axios.get(url)
            .then(res => {
                if (res.data.length == 0) {
                    this.setState({ message: "No Assing Found!!!!" });
                }
                res.data.map(det => {
                    console.log(det.empID);
                    console.log(det.dob);
                    this.setState({
                        asnID: det.asnID,
                        empName: det.empName,
                        depName: det.empName,
                    });
                })
                console.log(res.data);
            }).catch((err) => {
                console.log(err)
            })
    }

    searchAssignByDepName(event) {
        event.preventDefault();
        // console.log(this.state.empName);
        var url = 'http://localhost:3000/asn/search-employeesByDepName/' + this.state.depName;
        console.log(url);
        axios.get(url)
            .then(res => {
                if (res.data.length == 0) {
                    this.setState({ message: "No Assing Found!!!!" });
                }
                res.data.map(det => {
                    console.log(det.empID);
                    console.log(det.dob);
                    this.setState({
                        asnID: det.asnID,
                        empName: det.empName,
                        depName: det.depName,
                    });
                })
                console.log(res.data);
            }).catch((err) => {
                console.log(err)
            })
    }

    updateAssignDet() {
        axios.put('http://localhost:3000/asn/update-assignDet', {
            asnID: this.state.asnID,
            empName: this.state.empName,
            depName: this.state.depName,
            edited_usr: "nandun",
        })
            .then(response => {
                console.log(response);
                this.setState({ message: response.data });
            })
            .catch(err => {
                console.log(err);
            })
    }

    removeAssign() {
        axios.put('http://localhost:3000/asn/remove-assign', {
            asnID: this.state.asnID,
        })
            .then(response => {
                console.log(response);
                this.setState({ message: response.data });
            })
            .catch(err => {
                console.log(err);
            });
    }
    getAllAssign(){
        var url = 'http://localhost:3000/asn/get-assigns';
        console.log(url);
        axios.get(url)
        .then(res => {  
               console.log(res.data);
                this.setState({assigns: res.data});
                //console.log(this.state.assigns);
                this.geneateAssignTable();
            }).catch((err) => {
                console.log(err)
            })
    }

    geneateAssignTable() {
        {
            console.log('kjhdfkjs');
            console.log(this.state.assigns)
            return (
                 this.state.assigns.map(item =>
                    <tr>
                        <th>{item.asnID}</th>
                        <td>{item.empName}</td>
                        <td>{item.depName}</td>
                        {/* <td><span id="remove"><MdClear size={24} color="red" onClick={this.removeFromCart.bind(item)} /></span></td> */}

                    </tr>
                 )
            )
        }
    }

    change = e => {

        this.setState({
            [e.target.name]: e.target.value
        });
        //console.log(this.state.empName)
    };
    //for date 
    handleDate(event, date) {
        this.setState({ dob: date })
    }
    //for select field role
    handleChangeRole = (event, index, role) => this.setState({ role });
    //for select field gender
    handleChangeGender = (event, index, gender) => this.setState({ gender });

    //****************************** for password field */
    handleMouseDownPassword = event => {
        event.preventDefault();
    };

    handleClickShowPassword = () => {
        this.setState({ showPassword: !this.state.showPassword });
    };
    //**************************************************************

    render() {


        const styles = {
            errorStyle: {
                color: orange500,
            },
            underlineStyle: {
                borderColor: "#01579B",
            },
            floatingLabelStyle: {
                color: "#212121",
            },
            floatingLabelFocusStyle: {
                color: "#212121",
            },
            butyButton: {
                background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                width: 250,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .30)',
            }
        };

        return (
            <div>
                <Row>
                    <Col md={10} xs={12}>
                        <div className='mainGrid'>
                            <Card className="card">
                                <CardHeader>
                                    <h3>  Assign  Details</h3>
                                </CardHeader>
                                <CardBody>

                                    <div className="text_feild_items">
                                        <div className="inlineBUtton">
                                            <TextField
                                                name="asnID"
                                                hintText="Assign ID"
                                                floatingLabelText="Assign ID"
                                                underlineStyle={styles.underlineStyle}
                                                errorStyle={styles.errorStyle}
                                                value={this.state.asnID}
                                                onChange={e => this.change(e)}


                                            /><div id="serachBut">
                                                <Button
                                                    label="Search Assign"
                                                    labelPosition="before"
                                                    primary={true}
                                                    // style={styles.butyButton}
                                                    onClick={this.searchEmpByAssignID}

                                                />
                                            </div>
                                        </div>
                                        <br />
                                        <div className="inlineBUtton">
                                        <TextField
                                                name="empName"
                                                hintText="Employee Name"
                                                floatingLabelText="Employee Name"
                                                underlineStyle={styles.underlineStyle}
                                                errorStyle={styles.errorStyle}
                                                value={this.state.empName}
                                                onChange={e => this.change(e)}


                                            /><div id="serachBut">
                                                <Button
                                                    label="Search Assign"
                                                    labelPosition="before"
                                                    primary={true}
                                                    // style={styles.butyButton}
                                                    onClick={e => this.searchEmpByEmpName(e)}

                                                /></div>
                                        </div>
                                        <div className="inlineBUtton">
                                            <TextField
                                                name="depName"
                                                hintText="Enter Department Name"
                                                floatingLabelText="Department Name"
                                                underlineStyle={styles.underlineStyle}
                                                errorStyle={styles.errorStyle}
                                                value={this.state.depName}
                                                onChange={e => this.change(e)}
                                            />
                                            <div id="serachBut">
                                                <Button
                                                    label="Search Assign"
                                                    labelPosition="before"
                                                    primary={true}
                                                    // style={styles.butyButton}
                                                    onClick={e => this.searchAssignByDepName(e)}

                                                /></div></div>



                                        {/* <SelectField
                                            floatingLabelText="Gender"
                                            value={this.state.gender}
                                            onChange={this.handleChangeGender}

                                            underlineStyle={styles.underlineStyle} >
                                            <MenuItem value={"male"} primaryText="Male" />
                                            <MenuItem value={"female"} primaryText="Female" />


                                        </SelectField> */}

                                        <br />
                                        <br />
                                        <div className="beutyButDivUpper">
                                            <Button
                                                label="Add Assign"
                                                labelPosition="before"
                                                primary={true}

                                                onClick={this.addAssign}

                                            />
                                            <Button
                                                label="Update Assign"
                                                labelPosition="before"
                                                primary={true}

                                                onClick={this.updateAssignDet}

                                            />
                                            {(4 < this.state.uAccessLevel) &&
                                                <Button
                                                    label="Remove Assign"
                                                    labelPosition="before"
                                                    primary={true}

                                                    onClick={this.removeAssign}

                                                />
                                            }
                                            <Button
                                                label="Clear"
                                                labelPosition="before"
                                                primary={true}

                                                onClick={this.clearTextFieldsButton}

                                            />


                                        </div>

                                        <br />
                                        <h3>{this.state.message}</h3>

                                    </div>
                                </CardBody>
                            </Card>

                            <Card className="cardTab">
                                <CardHeader>
                                    <h3 align="center"> Assign Details</h3>
                                </CardHeader>
                                <CardBody>

                                    <Table border="1px">
                                        <thead>
                                            <tr>
                                                <th align="left">Assign ID</th>
                                                <th align="left">Employee Name</th>
                                                <th align="left">Department Name</th>
                                                

                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.geneateAssignTable()
                                            }

                                        </tbody>
                                    </Table>

                                </CardBody>
                            </Card>
                        </div>

                    </Col>

                </Row>



            </div>

        )
    }

}
export default Assign;