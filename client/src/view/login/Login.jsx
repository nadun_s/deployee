import React, { Component } from 'react';
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import '../../assets/css/login.css';
import axios from 'axios'
import Button from '@material-ui/core/Button';
import { orange500, blue500, cyan800, grey900 } from 'material-ui/styles/colors';
export default class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
        }
        this.loginFunc = this.loginFunc.bind(this)
    }

    loginFunc() {
        var self = this;

        axios.post('/login', {
            username: this.state.username,
            password: this.state.password
        })
            .then((res) => {
                console.log(res.data)
                sessionStorage.setItem("token", res.data.token)
                sessionStorage.setItem("empid", this.state.username)
                sessionStorage.setItem("role", res.data.role)
                sessionStorage.setItem("accesLevel", res.data.accesslvl)

                window.location.reload();
            })
    }
    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }


    render() {
        const styles = {

            card: {
                maxWidth: 1000,
            },
            media: {
                height: 0,
                paddingTop: '56.25%', // 16:9
            },
            errorStyle: {
                color: orange500,
            },
            underlineStyle: {
                borderColor: "#01579B",
            },
            floatingLabelStyle: {
                color: "#212121",
            },
            floatingLabelFocusStyle: {
                color: "#212121",
            },
            butyButton: {
                background: 'linear-gradient(45deg, #24C6DC 30%, #514A9D 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                width: 250,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .30)',
            }
        };
        return (
            <div id="fullscreen_bg" className="fullscreen_bg">
                <div className="form-signin">
                    <div className="rowlg">
                        <h1 className="topic">Human Resource Management</h1>



                        <form onSubmit={this.handleSubmit} >
                            <h1 className="signin">Sign In</h1>

                            <div id="gocenter">
                                <FormGroup controlId="username" bsSize="large" >

                                    <FormControl

                                        type="text"
                                        value={this.state.username}
                                        onChange={this.handleChange}
                                        placeholder="Username"

                                    />

                                </FormGroup>

                            </div>
                            <div id="gocenter">
                                <FormGroup controlId="password" bsSize="large" >

                                    <FormControl
                                        value={this.state.password}
                                        onChange={this.handleChange}
                                        type="password"
                                        placeholder="Password"
                                    />
                                </FormGroup>

                            </div>
                            <div id="logingButton">
                                <FormGroup>

                                    <Button

                                        labelPosition="before"
                                        primary={false}
                                        style={styles.butyButton}
                                        onClick={this.loginFunc}
                                    >
                                        Login
          </Button>
                                </FormGroup>
                            </div>


                        </form>


                    </div>

                </div>
            </div>
        )
    }
}

