import React from 'react';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer'
import {
    Route,
    Switch,
    Redirect,
    BrowserRouter
} from 'react-router-dom';
import MyAccoutn from '../MyAccount/MyAccount'
import UserAccount from '../UserAccounts/UserAccounts'
import Home from '../Home/Home';

import hospitalAccount from '../hospitalAccounts/hospitalAccount';
import hospitalinfo from '../hospitalAccounts/hospital-info';

import EmployeeMan from '../EmployeeManagement/EmployeeMan'
import AssignMan from '../EmployeeManagement/Assign'    

class MainLayout extends React.Component {


    render(){
        return(
            <BrowserRouter>
           <div>
               
            <div className="header">
 <Header />
 </div>
<div className="content">


<Route path='/' component={Home} />
 <Route path='/my-account' component={MyAccoutn} />
 <Route path='/user-account' component={UserAccount} />

  <Route path='/hospital-account' component={hospitalAccount} />
 <Route path='/hospital-info' component={hospitalinfo} />

 <Route path='/emp-man' component={EmployeeMan} />
 <Route path='/emp-assign' component={AssignMan} />
    
 
 </div>   
                <div>
                    <Footer />
                    </div>

               </div>
               </BrowserRouter>
        )
    }

}
export default MainLayout;