import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import img from '../../assets/Image/hrPeople.jpg'
import { orange500, blue500, cyan800, grey900 } from 'material-ui/styles/colors';
import {
    Route,
    Switch,
    Redirect,
    BrowserRouter,
    Link
} from 'react-router-dom';
import UserAcn from '../../view/UserAccounts/UserAccounts'
var crd= true;
const styles = {

    card: {
        maxWidth: 1000,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    errorStyle: {
        color: orange500,
    },
    underlineStyle: {
        borderColor: "#01579B",
    },
    floatingLabelStyle: {
        color: "#212121",
    },
    floatingLabelFocusStyle: {
        color: "#212121",
    },
    butyButton: {
        background: 'linear-gradient(45deg, #24C6DC 30%, #514A9D 90%)',
        borderRadius: 3,
        border: 0,
        color: 'white',
        height: 48,
        width: 250,
        padding: '0 30px',
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .30)',
    }
};
function SimpleMediaCard(props) {
    const { classes } = props;

//user accoutn
    function UserAcntFunc(){
       if(sessionStorage.getItem("accesLevel")>9){
        // crd =false;
        <Redirect to='/my-account' />
       }
       else{
        <Redirect to='/' />
           alert("You don not have permission for this page.")
         
       }
    }

    function depInforFunc(){
        // crd =false;
        <Redirect to='/hospital-account' />
    }

    function GenInfoFunc(){
        // crd =false;
        <Redirect to='/hospital-info' />
    }
    function EmpifoFunc(){
        // crd =false;
        <Redirect to='/emp-man' />
    }
    function assignInfoFunc(){
        // crd =false;
        <Redirect to='/emp-assign' />
    }
    return (
        <div>
{    crd &&        <Card className={classes.card}>
                <CardMedia
                    className={classes.media}
                    image={img}
                    title="Human Resource Management"
                />
                <CardContent>
                    <Typography gutterBottom variant="headline" component="h2">
                        Hospital Human Resources
          </Typography>
                    <Typography component="p">
               <b> They may forget your name, but they will never forget how you made them feel.</b>
                <br/>
             <i> Maya Angelou</i>
             <br/>
             <br/>
             We aren’t just looking for people with a certain certification or degree. We are also looking 
             for people with the passion and intrinsic motivation that makes them view their work as more than 
             simply a job.
              
          </Typography>
                </CardContent>
                <CardActions>
                <Button
                                                           
                                                           labelPosition="before"
                                                           primary={false}
                                                           style={styles.butyButton}
                                                           onClick={GenInfoFunc}
                                                       >
                       <Link to='/hospital-info'> General Information </Link>
          </Button>
          <Button
                                                           
                                                           labelPosition="before"
                                                           primary={false}
                                                           style={styles.butyButton}
                                                           onClick={depInforFunc}
                                                       >
               <Link to='/hospital-account'>  Department Infromation</Link>
          </Button>
                                                     <Button
                                                           
                                                            labelPosition="before"
                                                            primary={false}
                                                            style={styles.butyButton}
                                                            onClick={EmpifoFunc}
                                                        ><Link to='/emp-man'>Employee Management</Link>
                                                        </Button>
                                                        <Button
                                                           
                                                           labelPosition="before"
                                                           primary={false}
                                                           style={styles.butyButton}
                                                           onClick={assignInfoFunc}
                                                       ><Link to='/emp-assign'>Employee Assignation</Link>
                                                       </Button>
                                                                        
                                                         <Button
                                                            // label="Enable Hot Plug"
                                                            labelPosition="before"
                                                            primary={true}
                                                            style={styles.butyButton}
                                                            onClick={UserAcntFunc}
                                                        ><Link to='/user-account'>User Accounts</Link>
                                                        </Button>
                                                                        
                </CardActions>
            </Card>}
        </div>
    );
}

SimpleMediaCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleMediaCard);