import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import {orange500, blue500,cyan800,grey900} from 'material-ui/styles/colors';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import red from '@material-ui/core/colors/red';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Home from '../../view/Home/Home'
import {
  Route,
  Redirect,
  Link
} from 'react-router-dom';
// const styles = {
//   root: {
//     flexGrow: 1,
//   },
//   flex: {
//     flex: 1,
//   },
//   menuButton: {
//     marginLeft: -12,
//     marginRight: 20,
//   },
//   root: {
//     display: 'flex',
//     justifyContent: 'center',
//     alignItems: 'flex-end',
//   },
//   icon: {
//     margin: theme.spacing.unit * 2,
//   },
//   iconHover: {
//     margin: theme.spacing.unit * 2,
//     '&:hover': {
//       color: red[800],
//     },
//   }
// };

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  icon: {
    margin: theme.spacing.unit * 2,
  },
  iconHover: {
    margin: theme.spacing.unit * 2,
    '&:hover': {
      color: red[800],
    },
  },
  root: {
        flexGrow: 1,
      },
      flex: {
        flex: 1,
      },
      menuButton: {
        marginLeft: -12,
        marginRight: 20,
      },
      root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end',
      },
      icon: {
        margin: theme.spacing.unit * 2,
      },
      iconHover: {
        margin: theme.spacing.unit * 2,
        '&:hover': {
          color: red[800],
        },
      },
      menuItem: {
        '&:focus': {
          backgroundColor: theme.palette.primary.main,
          '& $primary, & $icon': {
            color: theme.palette.common.white,
          },
        },
      },
      primary: {},
      icon: {},
});

class MenuAppBar extends React.Component {
  state = {
    auth: true,
    anchorEl: null,
    anchorMenu: null,
  };

  handleChange = (event, checked) => {
    this.setState({ auth: checked });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };
  handleMenuMain = event => {
    this.setState({ anchorMenu: event.currentTarget });
  };


  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  handleClose_menu = () => {
    this.setState({ anchorMenu: null });
  };
  loadHome(){
    <Route path='/' component={Home} />
  }
  
  render() {
    const { classes } = this.props;
    const { anchorEl } = this.state;
    const { anchorMenu } = this.state;
    const open = Boolean(anchorEl);
    const openMenu =Boolean(anchorMenu);

    return (
      <div className={classes.root}>
    
        <AppBar position="fixed" color="primary" >
          <Toolbar>
          <div>
            <IconButton 
              aria-owns={open ? 'menu-main' : null} color="inherit" 
              aria-haspopup="true"
            onClick={this.handleMenuMain}
          
            // aria-owns={open ? 'menu-appbar' : null}
            >
              <MenuIcon />
            </IconButton>
            <Menu
                  id="menu-main"
                  anchorEl={anchorMenu}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                  }}
                  open={openMenu}
                  onClose={this.handleClose_menu}
                >
                  <MenuItem onClick={this.handleClose_menu}>
                  <ListItemIcon className={classes.icon}>
                  <InboxIcon className={classes.iconHover} color="error" style={{ fontSize: 30 }} />
                  </ListItemIcon><Link to="/user-account">
                  <ListItemText classes={{ primary: classes.primary }} inset primary="User Account Handling" />
                  </Link>
                  </MenuItem>
                  <MenuItem onClick={this.handleClose_menu}>General Information</MenuItem>
                  <MenuItem onClick={this.handleClose_menu}>Department Information</MenuItem>
                  <MenuItem onClick={this.handleClose_menu}>Attendence Management</MenuItem>
                  <MenuItem onClick={this.handleClose_menu}>Department Allocation</MenuItem>
                </Menu>
                </div>
            <Typography variant="title" color="inherit" className={classes.flex}>
          <Link to="/"  onClick={this.loadHome}>    Hospital HR Management System</Link>
            </Typography>
            
         
              <div>
                <IconButton
                  aria-owns={open ? 'menu-appbar' : null}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="inherit"
                >
                  <AccountCircle />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                  <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                  <MenuItem onClick={this.handleClose}>My account</MenuItem>
                </Menu>
              </div>
         
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

MenuAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuAppBar);