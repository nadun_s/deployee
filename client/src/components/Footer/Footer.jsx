import React from 'react';
import { Container } from 'reactstrap';
import PropTypes from 'prop-types';
import '../../assets/css/cStyle.css'

class Footer extends React.Component{
    render(){
        return (
            <footer className={"footer"}>
                <Container fluid={this.props.fluid ? true:false}>
                    
                    <div className="copyright">
                        &copy; {1900 + (new Date()).getYear()},  <a href="" target="_blank" rel="noopener noreferrer">Hospital</a> HR Management 
                        </div>
                </Container>
            </footer>
        );
    }
}

Footer.propTypes = {
    default: PropTypes.bool,
    fluid: PropTypes.bool
}

export default Footer;
