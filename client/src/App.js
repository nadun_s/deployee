import React, { Component } from 'react'

// Material-UI
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'


// Theme
import { deepOrange500 ,orange500, blue500,cyan800,grey900,darkBlack,pinkA200, minBlack} from 'material-ui/styles/colors'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import MainVies from './view/layout/mainLayout';
import Login from './view/login/Login'

// Font
import 'typeface-roboto'
import {
  Route,
  Switch,
  Redirect,
  BrowserRouter
} from 'react-router-dom';
// Click handler
import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()


// Styles
const styles = {
  container: {
    textAlign: 'center',
    paddingTop: 200
  }
}

// Theme

const muiTheme = getMuiTheme({
  fontFamily: 'Roboto, sans-serif',
  palette: {
    accent1Color: deepOrange500,
    textColor: darkBlack,
    primary2Color: darkBlack,
    primary3Color: darkBlack,
    disabledColor:cyan800,
    shadowColor: darkBlack,
  }

})

class App extends Component {
  constructor (props, context) {
    super(props, context)
    this.state={
      auth:false,
    }

    this.login_front = this.login_front.bind(this)
  }
   login_front() {
    if (sessionStorage.getItem("token")!=null) {
      return (

        <switch>
          <Redirect to='/' />
          <Route
            path='/'
            component={MainVies}
          />

        </switch>


      )
    }
    else {
      return (
        <switch>
          <Redirect to='/login' />
          <Route
            path='/login'
            component={Login}
          />
        </switch>
      )
    }
  }
  render () {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
     <BrowserRouter>


    {this.login_front()}


        </BrowserRouter>
      </MuiThemeProvider>
    )
  }
}

export default App
