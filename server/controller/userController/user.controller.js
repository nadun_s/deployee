
const UserModel = require('../../model/user.model.js');
const EmpCtr = require('../employeeController/employee.controller')
var obj=[];
 function getObjectIDofEmployee(empid){
  EmpCtr.searchEmployeeByEmpId(empid)
  .then((data) => {
    obj.push(data.employee[0]._id)
}).catch((err) => {
   return null;
});
}


var UserController = function () {
  // insert users
  this.insertUser = (data) => {
    return new Promise((resolve, reject) => {
      
      const urs = new UserModel({
        empID: data.empID,
        name: data.name,
        password: data.password,
        role: data.role,
        accessLevel: data.accessLevel,
        employee:  '5b250056f111804ed4bd432c',
        created_date:new Date(),
        created_usr:data.created_usr

      });
      urs.save().then(() => {
        resolve({ status: 200, message: "New user is added." })
      }).catch((err) => {
        reject({ status: 500, message: "Error : " + err })
      })
    });
  }
  getObjectIDofEmployee('Hs2')
  console.log(obj[0])
  //search all users
  this.searchAllUser = () => {
   
    return new Promise((resolve, reject) => {
      UserModel.find().populate("employee").exec().then((data) => {
        resolve({ status: 200, Userdata: data, message: "Sucess" })
      }).catch((err) => {
        reject({ status: 500, message: "Error: " + err.message })
      })
    })
  }

  //search user by userID
  this.searchUser = (userID) => {
    return new Promise((resolve, reject) => {
      UserModel.find({ empID: userID }).populate("employee").exec().then((data) => {
        if (Object.keys(data).length == 0) {
          resolve({ status: 204, Userdata: data, message: "No user found" })
        }
        else {
          resolve({ status: 200, Userdata: data, message: "User found" })
         
        }

      }).catch((err) => {
      
        reject({ status: 500, Userdata: null, message: "Error: " + err.message })
      })
    })
  }
  //Update user
  this.updateUser = function (userID, data) {
    return new Promise(function (resolve, reject) {
      UserModel.update({ empID: userID }, data).exec().then(function () {
        resolve({ status: 200, message: "User updated!" });
      }).catch(function (err) {
        reject({ status: 500, message: "Error: " + err });
      })
    })
  }
  //delete user
  this.deleteUser = function (userID) {
    return new Promise(function (resolve, reject) {
      UserModel.remove({ empID: userID }).then(function () {
        resolve({ status: 200, message: "User deleted!" });
      }).catch(function (err) {
        reject({ status: 404, message: "Error: " + err });
      })
    })
  }

  //deactivate user
  this.deactivateAccount = function (userID,data) {
    return new Promise(function (resolve, reject) {
      UserModel.update({ empID: userID },data ).exec().then(function () {
        resolve({ status: 200, message: "User deactivated!" });
      }).catch(function (err) {
        reject({ status: 500, message: "Error: " + err });
      })
    })
  }

    //activate user
    this.activateAccount = function (userID,data) {
      return new Promise(function (resolve, reject) {
        UserModel.update({ empID: userID }, data).exec().then(function () {
          resolve({ status: 200, message: "User activated!" });
        }).catch(function (err) {
          reject({ status: 500, message: "Error: " + err });
        })
      })
    }

};

module.exports = new UserController();

