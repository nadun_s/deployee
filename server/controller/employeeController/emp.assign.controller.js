const assignSchema = require('../../model/emp.assign.model');
//const departmentSchema = require('../../model/hospital.department');

var assignController = function () {
    this.addAssign = (data,id) => {
        //console.log(data);
        var timestmp = new Date().toString();
        // console.log(timestmp);
        var date = timestmp.split('G');
        // console.log(date);
        return new Promise((resolve, reject) => {
            var assign = new assignSchema({
                //asnID: id.newId,
                asnID: id.newId,
                empName: data.empName,
                depName: data.depName,
                created_date: date[0],
                created_usr: data.created_usr,
                edited_date: null,
                edited_usr: null,
                status: true
            });
            assign.save().then(() => {
                resolve({ status: 200, message: "Employee has been Assigned." });
            }).catch((err) => {
                console.log(err);
                reject({ status: 500, message: "Error" + err });
            });
        });
    }

    this.getAllAssigns = () => {
        return new Promise((resolve, reject) => {
            assignSchema.find({ status: true }).exec().then((data) => {
                resolve({ status: 200, assigns: data });
            }).catch((err) => {
                reject({ status: err.status, message: "Error" + err });
            });
        });
    }

    this.searchAssignByEmpName = (name) => {
        return new Promise((resolve, reject) => {
            console.log(name);
            assignSchema.find({ empName: name, status: true }).then((data) => {
                console.log(data);
                resolve({ status: 200, assigDet: data });
            }).catch((err) => {
                console.log(err);
                reject({ status: 404, message: "Error" + err })
            });
        });
    }

    this.searchAssignByDep = (depName) => {
        return new Promise((resolve, reject) => {
            // console.log(name);
            assignSchema.find({ depName: depName, status: true }).then((data) => {
                resolve({ status: 200, assignDet: data });
            }).catch((err) => {
                console.log(err);
                reject({ status: 404, message: "Error" + err })
            });
        });
    }

    this.searchAssignByAsignId = (id) => {
        return new Promise((resolve, reject) => {
            // console.log(name);
            assignSchema.find({ asnID: id, status: true }).then((data) => {
                resolve({ status: 200, assignDet: data });
            }).catch((err) => {
                console.log(err);
                reject({ status: 404, message: "Error" + err })
            });
        });
    }

    this.updateAssignDet = (data) => {
        return new Promise((resolve, reject) => {
            var timestmp = new Date().toString();
            var date = timestmp.split('G');
            assignSchema.update({ asnID: data.asnID },
                {
                    empName: data.empName,
                    depName: data.depName,
                    edited_date: date[0],
                    edited_usr: data.edited_usr
                }
            ).exec().then(() => {
                resolve({ status: 200, message: "Assign Data has been Updated." });
            }).catch((err) => {
                console.log(err);
                reject({ status: 500, message: "Error" + err });
            });
        });
    }

    this.removeAssign = (data) => {
        return new Promise((resolve, reject) => {
            assignSchema.update({ asnID: data.asnID }, { status: false }).exec().then(() => {
                resolve({ status: 200, message: "Assign has been Removed." });
            }).catch((err) => {
                reject({ status: 500, message: "Error" + err });
            });
        });
    }

    this.getNextAssgnID = () => {
        return new Promise((resolve, reject) => {
            assignSchema.find().limit(1).sort({ $natural: -1 }).then((data) => {
                // var lastrec = JSON.stringify(data);
                data.map((item) => {
                    // console.log(item.empID);
                    var splitId = item.asnID.toString().split('A');
                    // console.log(splitId)
                    var intId = parseInt(splitId[1], 10) + 1;
                    //console.log(intId +"newid");
                    var stringid = intId.toString();
                    console.log(stringid);
                    var newId = "HSA" + stringid;
                    console.log(newId);
                    resolve({ status: 200, newId: newId });
                });


            }).catch((err) => {
                console.log(err);
                reject({ status: 404, message: "Error" + err })
            });
        }); n
    }
}

module.exports = new assignController();