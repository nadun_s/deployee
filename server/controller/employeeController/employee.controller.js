const employeeSchema = require('../../model/employee.model');

var EmpConstroller = function () {
    this.addEmployee = (data,id) => {
        //console.log(data);
        var timestmp = new Date().toString();
        // console.log(timestmp);
        var date = timestmp.split('G');
        // console.log(date);
        var datepickerDob = data.dob.toString();
            console.log(datepickerDob);
            var dob = datepickerDob.split('T');
            console.log(dob);
        return new Promise((resolve, reject) => {
            var employee = new employeeSchema({
                empID: id.newId,
                name: data.name,
                nic: data.nic,
                dob: dob[0],
                // doj: data.doj,
                designation: data.designation,
                // role: data.role,
                com_address: data.com_address,
                // perm_address: data.perm_address,
                // cur_pto: data.cur_pto,
                con_no: data.con_no,
                email: data.email,
                salary: data.salary,
                created_date: date[0],
                created_usr: data.created_usr,
                edited_date: null,
                edited_usr: null,
                status: true
            });
            employee.save().then(() => {
                resolve({ status: 200, message: "Employee has been Added Successfully." });
            }).catch((err) => {
                console.log(err);
                reject({ status: 500, message: "Error" + err });
            });
        });
    }

    this.getAllEmployees = () => {
        return new Promise((resolve, reject) => {
            employeeSchema.find({status:true}).exec().then((data) => {
                resolve({ status: 200, employees: data });
            }).catch((err) => {
                reject({ status: err.status, message: "Error" + err });
            });
        });
    }

    this.searchEmployeeByName = (name) => {
        return new Promise((resolve, reject) => {
            console.log(name);
            employeeSchema.find({ name: name, status:true}).then((data) => {
                resolve({ status: 200, employee: data });
            }).catch((err) => {
                reject({ status: 404, message: "Error" + err })
            });
        });
    }

    this.searchEmployeeByNIC = (nic) => {
        return new Promise((resolve, reject) => {
            // console.log(name);
            employeeSchema.find({ nic: nic, status:true }).then((data) => {
                resolve({ status: 200, employee: data });
            }).catch((err) => {
                reject({ status: 404, message: "Error" + err })
            });
        });
    }

    this.updateEmloyeeDet = (data) => {
        return new Promise((resolve, reject) => {
            var timestmp = new Date().toString();
            var date = timestmp.split('G');

            var datepickerDob = data.dob.toString();
            console.log(datepickerDob);
            var dob = datepickerDob.split('T');
            console.log(dob);
            employeeSchema.update({ empID: data.empID }, 
                {
                    name: data.name,
                    nic: data.nic,
                    dob: dob[0],
                    doj: data.doj,
                    designation: data.designation,
                    role: data.role,
                    com_address: data.com_address,
                    perm_address: data.perm_address,
                    cur_pto: data.cur_pto,
                    con_no: data.con_no,
                    email: data.email,
                    salary: data.salary,
                    edited_date: date[0],
                    edited_usr: data.edited_usr
                }
            ).exec().then(() => {
                resolve({ status: 200, message: "Employee Data has been Updated." });
            }).catch((err) => {
                console.log(err);
                reject({ status: 500, message: "Error" + err });
            });
        });
    }

    this.removeEmployee = (data) => {
        return new Promise((resolve, reject) => {
            employeeSchema.update({ empID: data.empID }, { status: false }).exec().then(() => {
                resolve({ status: 200, message: "Employee has been Removed." });
            }).catch((err) => {
                reject({ status: 500, message: "Error" + err });
            });
        });
    }

    this.getNextEmployeeId = () => {
        return new Promise((resolve, reject) => {
            employeeSchema.find().limit(1).sort({ $natural: -1 }).then((data) => {
                // var lastrec = JSON.stringify(data);
                data.map((item) => {
                    // console.log(item.empID);
                    var splitId = item.empID.toString().split('S');
                    // console.log(splitId)
                    var intId = parseInt(splitId[1], 10) + 1;
                    //console.log(intId +"newid");
                    var stringid = intId.toString();
                    console.log(stringid);
                    var newId = "HS" + stringid;
                    console.log(newId);
                    resolve({ status: 200, newId: newId });
                });


            }).catch((err) => {
                reject({ status: 404, message: "Error" + err })
            });
        });
    }

    this.searchEmployeeByEmpId = (empID) => {
        return new Promise((resolve, reject) => {
            // console.log(name);
            employeeSchema.find({ empID: empID, status:true }).then((data) => {
                resolve({ status: 200, employee: data });
            }).catch((err) => {
                reject({ status: 404, message: "Error" + err })
            });
        });
    }
}

module.exports = new EmpConstroller();