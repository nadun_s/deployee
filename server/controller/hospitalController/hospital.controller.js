var mongoose = require('../../model/hospital.model');
var Hospitalmod = mongoose.model('Dep');

var HospitalcontrollerMain = function() {
  //insert department details

  var d = new Date();

  this.insertDetails = (data,id) => {
    return new Promise((resolve, reject) => {
      var hosp = new Hospitalmod({
        did: id.stringid,
        depname: data.depname,
        no_of_wards: data.no_of_wards,
        no_of_max_patients: data.no_of_wards,
        allocation: data.allocation,
        phonenumber: data.phonenumber,
        email: data.email,
        head_doctor: data.head_doctor,
        status: true,
        services: data.services,
        create_date: d,
        create_user: data.create_user,
        edited_date: null,
        edited_user: null
      });
      hosp
        .save()
        .then(() => {
          resolve({ status: 200, message: 'New record is added.' });
        })
        .catch(err => {
          console.log(err);
          reject({ status: 500, message: 'Error' + err });
        });
    });
  };
  this.getalldepartments = () => {
    return new Promise((resolve, reject) => {
      Hospitalmod.find({ status: true })
        .exec()
        .then(data => {
          resolve({ status: 200, departments: data });
        })
        .catch(err => {
          reject({ status: 500, message: 'error' + err });
        });
    });
  };
  this.searchDepartmentByname = depname => {
    return new Promise((resolve, reject) => {
      Hospitalmod.find({ depname: depname })
        .exec()
        .then(data => {
          resolve({ status: 200, deparment: data });
        })
        .catch(err => {
          reject({ status: 404, message: 'error' + err });
        });
    });
  };

  this.updatedep = (id, data) => {
    var d = new Date();

    return new Promise((resolve, reject) => {
      Hospitalmod.update(
        { did: id },
        {
          did: data.did,
          depname: data.depname,
          no_of_wards: data.no_of_wards,
          no_of_max_patients: data.no_of_wards,
          allocation: data.allocation,
          phonenumber: data.phonenumber,
          email: data.email,
          head_doctor: data.head_doctor,
          status: true,
          services: data.services,
          edited_date: d,
          edited_user: data.edited_user
        }
      )
        .exec()
        .then(() => {
          resolve({ status: 200, message: 'details udated succesfully' });
        })
        .catch(err => {
          reject({ status: 500, message: 'error' + err });
        });
    });
  };

  this.deletedep = id => {
    return new Promise((resolve, reject) => {
      Hospitalmod.update(
        { did: id },
        {
          $set: {
            status: false
          }
        }
      )
        .then(() => {
          resolve({ status: 200, message: 'deparment deleted' });
        })
        .catch(err => {
          reject({ status: 404, message: 'User not found' + err });
        });
    });
  };


  this.getNextdepID = () => {
    console.log('kkkk');
    return new Promise((resolve, reject) => {
        Hospitalmod.find().limit(1).sort({ $natural: -1 }).then((data) => {
          
            data.map((item) => {
                var intId = parseInt(item.did) + 1;
               
                var stringid = intId.toString();
                console.log(stringid);
               
                resolve({ status: 200, stringid: stringid });
            });


        }).catch((err) => {
            reject({ status: 404, message: "Error" + err })
        });
    }); 
}
};
module.exports = new HospitalcontrollerMain();
