var mongoose = require('../../model/hos.gen.info');
var Hospitalmod = mongoose.model('General');

HospitalcontrollerMain = function() {
  //insert department details

  var d = new Date();

  this.insertDetails = (data,id) => {
    return new Promise((resolve, reject) => {
      var hosp = new Hospitalmod({
        branch_id:id.stringid ,
        hos_name: data.hos_name,
        no_of_staffs: data.no_of_staffs,
        hos_no: data.hos_no,
        hos_email: data.hos_email,
        address: data.address,
        status: true,
        create_date: d,
        create_user: data.create_user,
        edited_date: null,
        edited_user: null
      });
      hosp
        .save()
        .then(() => {
          resolve({ status: 200, message: 'New record is added.' });
        })
        .catch(err => {
          reject({ status: 500, message: 'Error' + err });
        });
    });
  };
  this.getallinfo = () => {
    return new Promise((resolve, reject) => {
      Hospitalmod.find({ status: true })
        .exec()
        .then(data => {
          resolve({ status: 200, info: data });
        })
        .catch(err => {
          reject({ status: 500, message: 'error' + err });
        });
    });
  };
  this.searchinfoByname = hos_name => {
    return new Promise((resolve, reject) => {
      Hospitalmod.find({ hos_name: hos_name })
        .exec()
        .then(data => {
          resolve({ status: 200, deparment: data });
        })
        .catch(err => {
          reject({ status: 404, message: 'error' + err });
        });
    });
  };

  this.updateinfo = (id, data) => {
    var d = new Date();

    return new Promise((resolve, reject) => {
      Hospitalmod.update(
        { hos_name: id },
        {
          branch_id: data.branch_id,
          hos_name: data.hos_name,
          no_of_staffs: data.no_of_staffs,
          hos_no: data.hos_no,
          hos_email: data.hos_email,
          address: data.address,
          edited_date: d,
          edited_user: data.edited_user
        }
      )
        .exec()
        .then(() => {
          resolve({ status: 200, message: 'details udated succesfully' });
        })
        .catch(err => {
          reject({ status: 500, message: 'error' + err });
        });
    });
  };

  this.deleteinfo = id => {
    return new Promise((resolve, reject) => {
      Hospitalmod.update(
        { hos_name: id },
        {
          $set: {
            status: false
          }
        }
      )
        .then(() => {
          resolve({ status: 200, message: 'general info deleted' });
        })
        .catch(err => {
          reject({ status: 404, message: 'details not found' + err });
        });
    });
  };
  this.getNextdepID = () => {
    return new Promise((resolve, reject) => {
        Hospitalmod.find().limit(1).sort({ $natural: -1 }).then((data) => {
          
            data.map((item) => {
               
              
               
                var intId = parseInt(item.branch_id) + 1;
                console.log(item.branch_id);
               
                var stringid = intId.toString();
                console.log(stringid);
               
                resolve({ status: 200, stringid: stringid });
            });


        }).catch((err) => {
            reject({ status: 404, message: "Error" + err })
        });
    }); 
}
};
module.exports = new HospitalcontrollerMain();
