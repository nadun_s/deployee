const mongoose = require('mongoose');
const assignSchema = mongoose.Schema;

var assign = new assignSchema({
    asnID: {
        type: String,
        required: [true, 'Assign ID is Required!']
    },
    empName: {
        type: String,
        required: [true, 'Employee Name is Required!']
    },
    depName: {
        type: String,
        required: [true, 'Departent Name is Required!']
    },
    created_date: String,
    created_usr: String,
    edited_date: String,
    edited_usr: String,
    status: Boolean
});

const assignmodel = mongoose.model('assign',assign);

module.exports = assignmodel;