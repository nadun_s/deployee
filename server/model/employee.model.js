const mongoose = require('mongoose');
const employeeSchema = mongoose.Schema;

var employee = new employeeSchema({
    empID: {
        type: String,
        required: [true, 'Employee ID is Required!']
    },
    name: {
        type: String,
        required: [true, 'Employee Name is Required!']
    },
    nic: {
        type: String,
        required: [true, 'Employee NIC is Required!']
    },
    dob: {
        type: String,
        required: [true, 'Employee Date of Birth is Required!']
    },
    // doj: {
    //     type: String,
    //     required: [true, 'Employee Date of joining is Required!']
    // },
    designation: {
        type: String,
        required: [true, 'Employee Description is Required!']
    },
    // role: {
    //     type: String,
    //     required: [true, 'Employee ID is Required!']
    // },
    com_address: {
        type: String,
        required: [true, 'Employee Communication Adress is Required!']
    },
    // perm_address: {
    //     type: String,
    //     required: [true, 'Employee Permanent Adress is Required!']
    // },
    // cur_pto: {
    //     data: Buffer,
    //     contentType: String 
    // },
    con_no: {
        type: String,
        required: [true, 'Employee Contact Number is Required!']
    },
    email: {
        type: String,
        required: [true, 'Employee E-Mail is Required!']
    },
    salary: {
        type: String,
        required: [true, 'Employee Salary is Required']
    },
    UserAccounts: {
        type: employeeSchema.Types.ObjectId
    },
    created_date: String,
    created_usr: String,
    edited_date: String,
    edited_usr: String,
    status: Boolean
});

const employeemodel = mongoose.model('employee', employee);

module.exports = employeemodel;