const mongoose = require('mongoose');
const schema = mongoose.Schema;

const Geninfo = new schema({
  branch_id: {
    type: String,
    required: [true, 'branch id is must']
  },
  hos_name: {
    type: String,
    required: [true, 'hospital name  name is must']
  },
  no_of_staffs: {
    type: Number,
    required: [true, 'no of staff   is must']
  },
  hos_no: {
    type: String,
    required: [true, 'hospital phone number is must']
  },
  hos_email: {
    type: String,
    required: [true, 'email is must']
  },
  address: {
    type: String,
    required: [true, 'address is must']
  },

  status: {
    type: Boolean
  },

  create_date: {
    type: String
  },
  create_user: {
    type: String
  },
  edited_date: {
    type: String
  },
  edited_user: {
    type: String
  }
});

mongoose.model('General', Geninfo);

module.exports = mongoose;
