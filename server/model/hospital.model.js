const mongoose = require('mongoose');
const schema = mongoose.Schema;

const DepartmentSchema = new schema({
  did: {
    type: String,
    required: [true, 'did is must']
  },
  depname: {
    type: String,
    required: [true, 'department name is must']
  },
  no_of_wards: {
    type: Number,
    required: [true, 'wards number is must']
  },
  no_of_max_patients: {
    type: Number,
    required: [true, 'patients number is must']
  },
  allocation: {
    type: String,
    required: [true, 'allocation is must']
  },
  phonenumber: {
    type: String,
    required: [true, 'phone number is must']
  },
  email: {
    type: String,
    required: [true, 'email is must']
  },
  head_doctor: {
    type: String,
    required: [true, 'head doctor is  must ']
  },

  status: {
    type: Boolean
  },
  services: {
    type: [String]
  },
  create_date: {
    type: String
  },
  create_user: {
    type: String
  },
  edited_date: {
    type: String
  },
  edited_user: {
    type: String
  }
});

mongoose.model('Dep', DepartmentSchema);

module.exports = mongoose;
