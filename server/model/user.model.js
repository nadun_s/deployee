const mongoose = require('mongoose');


var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;

const schema = mongoose.Schema;

const UserSchema = new schema({
  empID: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    required: true,
    default: 'guest'
  },
  accessLevel: {
    type: Number,
    required: true,
    default: 4
  },
  status: {
    type: Boolean,
    default: true
  },
  employee: {
    type: schema.Types.ObjectId,
    ref: 'employee'
  },
  created_date: String,
  created_usr: String,
  edited_date: String,
  edited_usr: String,
});

//encrypt passwords
    //saving
UserSchema.pre('save', function(next){
  var user = this;
  if (!user.isModified('password')) return next();

  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt){
      if(err) return next(err);

      bcrypt.hash(user.password, salt, function(err, hash){
          if(err) return next(err);

          user.password = hash;
          next();
      });
  });
});

    //updating
    UserSchema.pre('update', function(next){
      var user = this;
      //  if (!user.isModified('password')) return next();
    
      bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt){
          if(err){ 
            console.log('from block one : ',err)
            return next(err);}
 
          bcrypt.hash(user._update.password, salt, function(err, hash){
              if(err) {
                console.log('from block two : ',err)
                return next(err);
              }
    
              user.password = hash;
              next();
          });
      });
    });
//compare passwords
UserSchema.methods.comparePassword = function(password){
  return bcrypt.compareSync(password, this.password);
}
const userModel = mongoose.model('UserAccounts', UserSchema);
module.exports = userModel;
