module.exports = function(app) {
  var controller = require('../../controller/hospitalController/hospital.general.controller');
  
  app.route('/gen-information')
  .post((req, res) => {
      controller.getNextdepID().then((newid) => {
        console.log(newid + "inside post")
          controller.insertDetails(req.body,newid).then((data) => {
              res.status(data.status).send({ message: data.message });
          }).catch((err) => {
              console.log(err);
              res.status(err.status).send({ message: err.message });
          });
      }).catch((err) => {
          console.log(err);
          res.status(err.status).send({ message: err.message });
      });
  });

  app.route('/gen-information').get((req, res) => {
    controller
      .getallinfo()
      .then(data => {
        res.status(data.status).send({ info: data });
      })
      .catch(err => {
        res.status(err.status).send({ message: err.message });
      });
  });

  app.route('/gen-information/update/:name').put((req, res) => {
    controller
      .updateinfo(req.params.name, req.body)
      .then(data => {
        res.status(data.status).send({ message: data.message });
      })
      .catch(err => {
        res.status(err.status).send({ message: err.mesage });
      });
  });

  app.route('/gen-information/delete/:name').put((req, res) => {
    controller
      .deleteinfo(req.params.name)
      .then(data => {
        res.status(data.status).send({ message: data.message });
      })
      .catch(err => {
        res.status(err.status).send({ message: err.message });
      });
  });
  app.route('/gen-information/:name').get((req, res) => {
    controller
      .searchinfoByname(req.params.name)
      .then(data => {
        res.status(data.status).send({ deparment: data });
      })
      .catch(err => {
        res.status(data.status).send({ data: err.message });
      });
  });
  
  app.route('/info/get-newID')
  .get((req, res) => {
          controller.getNextdepID().then((data) => {
          res.status(data.status).send(data.stringid);
      }).catch((err) => {
          res.status(err.status).send(err.message);
      });
  });
};
