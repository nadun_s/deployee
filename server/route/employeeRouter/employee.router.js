module.exports = function (app) {

    var employeeController = require('../../controller/employeeController/employee.controller')
    app.route('/emp/add-employee')
        .post((req, res) => {
            employeeController.getNextEmployeeId().then((newid) => {
                employeeController.addEmployee(req.body,newid).then((data) => {
                    res.status(data.status).send({ message: data.message });
                }).catch((err) => {
                    console.log(err);
                    res.status(err.status).send({ message: err.message });
                });
            }).catch((err) => {
                console.log(err);
                res.status(err.status).send({ message: err.message });
            });
        });

        app.route('/emp/get-employees')
        .get((req, res) => {
            employeeController.getAllEmployees().then((data) => {
                res.status(data.status).send(data.employees);
            }).catch((err) => {
                res.status(err.status).send(err.message);
            });
        });

        app.route('/emp/search-employeesByname/:name')
        .get((req, res) => {
            employeeController.searchEmployeeByName(req.params.name).then((data) => {
                res.status(data.status).send(data.employee);
            }).catch((err) => {
                res.status(err.status).send(err.message);
            });
        });

        app.route('/emp/search-employeesBynic/:nic')
        .get((req, res) => {
            employeeController.searchEmployeeByNIC(req.params.nic).then((data) => {
                console.log(data);
                res.status(data.status).send(data.employee);
            }).catch((err) => {
                res.status(err.status).send(err.message);
            });
        });

        app.route('/emp/update-employees')
        .put((req, res) => {
            employeeController.updateEmloyeeDet(req.body).then((data) => {
                console.log(data);
                res.status(data.status).send(data.message);
            }).catch((err) => {
                console.log(err);
                res.status(err.status).send(err.message);
            });
        });

        app.route('/emp/remove-employees')
        .put((req, res) => {
            employeeController.removeEmployee(req.body).then((data) => {
                console.log(data);
                res.status(data.status).send(data.message);
            }).catch((err) => {
                console.log(err);
                res.status(err.status).send(err.message);
            });
        });

        app.route('/emp/get-newID')
        .get((req, res) => {
            console.log('sdfdfs')
            employeeController.getNextEmployeeId().then((data) => {
                res.status(data.status).send(data.newId);
            }).catch((err) => {
                res.status(err.status).send(err.message);
            });
        });

        app.route('/emp/search-employeesById/:id')
        .get((req, res) => {
            employeeController.searchEmployeeByEmpId(req.params.id).then((data) => {
                res.status(data.status).send(data.employee);
            }).catch((err) => {
                res.status(err.status).send(err.message);
            });
        });
};
