module.exports = function (app) {

    var assignController = require('../../controller/employeeController/emp.assign.controller')
    app.route('/asn/add-assign')
        .post((req, res) => {
            assignController.getNextAssgnID().then((newid) => {
                assignController.addAssign(req.body,newid).then((data) => {
                    res.status(data.status).send({ message: data.message });
                }).catch((err) => {
                    console.log(err);
                    res.status(err.status).send({ message: err.message });
                });
            }).catch((err) => {
                console.log(err);
                res.status(err.status).send({ message: err.message });
            });
        });

        app.route('/asn/get-assigns')
        .get((req, res) => {
            assignController.getAllAssigns().then((data) => {
                res.status(data.status).send(data.assigns);
            }).catch((err) => {
                res.status(err.status).send(err.message);
            });
        });

        app.route('/asn/search-assignsByEmpName/:name')
        .get((req, res) => {
            assignController.searchAssignByEmpName(req.params.name).then((data) => {
                res.status(data.status).send(data.assigDet);
            }).catch((err) => {
                console.log(err);
                res.status(err.status).send(err.message);
            });
        });

        app.route('/asn/search-employeesByDepName/:depName')
        .get((req, res) => {
            assignController.searchAssignByDep(req.params.depName).then((data) => {
                console.log(data);
                res.status(data.status).send(data.assignDet);
            }).catch((err) => {
                console.log(err);
                res.status(err.status).send(err.message);
            });
        });

        app.route('/asn/search-assignsByAssignId/:id')
        .get((req, res) => {
            assignController.searchAssignByAsignId(req.params.id).then((data) => {
                console.log(data);
                res.status(data.status).send(data.assignDet);
            }).catch((err) => {
                console.log(err);
                res.status(err.status).send(err.message);
            });
        });

        app.route('/asn/update-assignDet')
        .put((req, res) => {
            assignController.updateAssignDet(req.body).then((data) => {
                console.log(data);
                res.status(data.status).send(data.message);
            }).catch((err) => {
                console.log(err);
                res.status(err.status).send(err.message);
            });
        });

        app.route('/asn/remove-assign')
        .put((req, res) => {
            assignController.removeAssign(req.body).then((data) => {
                console.log(data);
                res.status(data.status).send(data.message);
            }).catch((err) => {
                console.log(err);
                res.status(err.status).send(err.message);
            });
        });

        app.route('/asn/get-newID')
        .get((req, res) => {
            assignController.getNextAssgnID().then((data) => {
                res.status(data.status).send(data.newId);
                console.log(data.newID);
            }).catch((err) => {
                console.log(err);
                res.status(err.status).send(err.message);
            });
        });
};
