module.exports = function(app){

    //user routes
    require('./userRouter/user.router')(app)
    require('./hospitalRoutes/hospital.route')(app)
    require('./hospitalRoutes/hospital.info')(app)
    require('./employeeRouter/employee.router')(app)
    require('./employeeRouter/assign.router')(app)
    
    
}