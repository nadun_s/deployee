module.exports = function (app) {
    const jwt = require('jwt-simple');
    var UserController = require('../../controller/userController/user.controller');
    const config = require('../../bin/config')
    var bcrypt = require('bcrypt');
    // Insert user details
    app.route('/add-user')  
        .post((req, res) => {
            UserController.insertUser(req.body)
                .then((data) => {
                    res.send(JSON.stringify({ "status": data.status, "message": data.message }))
                })
                .catch((err) => {
                    res.send(JSON.stringify({ "status": data.status, "message": err.message }))
                })
        })

    // Search all Users
    app.route('/get-user')
        .get((req, res) => {
            UserController.searchAllUser()
                .then((data) => {
                    res.send(JSON.stringify({ "status": data.status, "data": data.Userdata, "message": data.message }))
                })
        })

    //search user by userID
    app.route('/get-user/:id')
        .get((req, res) => {
            UserController.searchUser(req.params.id)
                .then((data) => {
                    
                    res.send(JSON.stringify({ "status": data.status, "data": data.Userdata[0], "message": data.message }))
                }).catch((err)=>{
                    console.log('This is fuckling erf ',err)
                })
        })
    //Update user by user id
    app.route('/edit-user/:id')
        .post((req, res) => {
            UserController.updateUser(req.params.id, req.body)
                .then((data) => {
                    res.send(JSON.stringify({ "status": data.status, "message": data.message }))
                })
                .catch((err) => {
                    res.send(JSON.stringify({ "status": err.status, "message": err.message }))
                })
        })

    //Delete user by id
    app.route('/delete-user/:id')
        .delete((req, res) => {
            UserController.deleteUser(req.params.id)
                .then((data) => {
                    res.send(JSON.stringify({ "status": data.status, "message": data.message }))
                })
                .catch((err) => {
                    res.send(JSON.stringify({ "status": data.status, "message": err.message }))
                })
        })

    //Deactivate user by user id
    app.route('/deactivate-user/:id')
        .post((req, res) => {
            console.log(req.body)
            UserController.deactivateAccount(req.params.id,req.body)
                .then((data) => {
                    res.send(JSON.stringify({ "status": data.status, "message": data.message }))
                })
                .catch((err) => {
                    res.send(JSON.stringify({ "status": data.status, "message": err.message }))
                })
        })

    //Activate user by user id
    app.route('/activate-user/:id')
        .post((req, res) => {
            console.log(req.body)
            UserController.activateAccount(req.params.id,req.body)
                .then((data) => {
                    res.send(JSON.stringify({ "status": data.status, "message": data.message }))
                })
                .catch((err) => {
                    res.send(JSON.stringify({ "status": data.status, "message": err.message }))
                })
        })


        //login
        var arr=[];
        app.route('/login')
        .post((req,res)=>{
            var username = req.body.username;
            var password = req.body.password;
            UserController.searchUser(username)
            .then((data)=>{
                // console.log(data.Userdata[0])
                arr.push(data.Userdata[0].accessLevel)
                 arr.push(data.Userdata[0].role)
                
                return bcrypt.compare(password, data.Userdata[0].password);
            }).then(function(matched) {
                if(!matched) {
                   res.send(JSON.stringify({"status":403,"token":null,"message":"Invalid Credentials!","accessLevel":arr}))
                }
                else{
                    
                    var token = jwt.encode({ username:username }, config.JWT_SECRET);
                    // console.log("authentication success",arr.accessLevel)
                    console.log(arr[1])
                    res.send(JSON.stringify({"status":200,"accesslvl":arr[0],"role":arr[1],"token":token,"message":"Authenticated"}))
                }
            })
            .catch(function(error){
                console.log("Error authenticating user: ");
                console.log(error);
                next();
            });
        })

}